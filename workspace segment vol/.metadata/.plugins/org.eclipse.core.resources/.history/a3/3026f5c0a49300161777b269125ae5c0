/*
 * @titre : antenne.h
 * @auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description : Dans ce fichier se trouvent les fonctions de commande/controle de la carte antenne ISIS.
 */

#include <stdint.h>

//Adresse des microcontroleur de la carte antenne ISIS
#define MC_A_ADDRESS 0x31 // Adresse du microcontroleur A
#define MC_B_ADDRESS 0x32 // Adresse du microcontroleur B

//Fonction de reset du microcontroleur
void resetMC(uint8_t mc_adress);

//Fonctions d'armement/desarmement
void armAntennas(uint8_t mc_adress);
void disarmAntennas(uint8_t mc_adress);

//Fonctions de deploiement des antennes
void deployAntenna_1(uint8_t mc_adress, uint8_t maxActivationTime);
void deployAntenna_2(uint8_t mc_adress, uint8_t maxActivationTime);
void deployAntenna_3(uint8_t mc_adress, uint8_t maxActivationTime);
void deployAntenna_4(uint8_t mc_adress, uint8_t maxActivationTime);

//Fonctions de deploiement automatique
void startAutomatedSequentialAntennaDeployment(uint8_t mc_adress, uint8_t maxActivationTime);

//Fonctions de deploiement avec override des antennes
void deployAntenna_1_withOverride(uint8_t mc_adress, uint8_t maxActivationTime);
void deployAntenna_2_withOverride(uint8_t mc_adress, uint8_t maxActivationTime);
void deployAntenna_3_withOverride(uint8_t mc_adress, uint8_t maxActivationTime);
void deployAntenna_4_withOverride(uint8_t mc_adress, uint8_t maxActivationTime);

//Fonctions d'annulation de deploiement des antennes
void cancelDeployment(uint8_t mc_adress, uint8_t maxActivationTime);

uint16_t reportDeploymentStatus(uint8_t mc_adress);

uint8_t reportAntenna_1_activationCount(uint8_t mc_adress);
uint8_t reportAntenna_2_activationCount(uint8_t mc_adress);
uint8_t reportAntenna_3_activationCount(uint8_t mc_adress);
uint8_t reportAntenna_4_activationCount(uint8_t mc_adress);

uint8_t reportAntenna_1_activationTime(uint8_t mc_adress);
uint8_t reportAntenna_2_activationTime(uint8_t mc_adress);
uint8_t reportAntenna_3_activationTime(uint8_t mc_adress);
uint8_t reportAntenna_4_activationTime(uint8_t mc_adress);

uint16_t measureTemperature(uint8_t mc_adress);
