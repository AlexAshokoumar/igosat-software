/*
 * @titre : antenne.c
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description :
 */
#include "antenne.h"
#include "mi2c.h"
#include "bitop.h"

//Fonction de reset du microcontroleur
void resetMC(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10101010);
	I2C_stop(I2C1);
}

//Fonctions d'armement/desarmement
void armAntennas(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10101101);
	I2C_stop(I2C1);
}

void disarmAntennas(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10101100);
	I2C_stop(I2C1);
}

//Fonctions de deploiement des antennes
void deployAntenna_1(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10100001);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

void deployAntenna_2(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10100010);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

void deployAntenna_3(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10100011);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

void deployAntenna_4(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10100100);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

//Fonctions de deploiement automatique
void startAutomatedSequentialAntennaDeployment(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10100101);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

//Fonctions de deploiement avec override des antennes
void deployAntenna_1_withOverride(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10111010);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

void deployAntenna_2_withOverride(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10111011);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

void deployAntenna_3_withOverride(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10111100);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

void deployAntenna_4_withOverride(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10111101);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

//Fonctions d'annulation de deploiement des antennes
void cancelDeployment(uint8_t mc_adress, uint8_t maxActivationTime){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10101001);
	I2C_write(I2C1,maxActivationTime);
	I2C_stop(I2C1);
}

/*************************** Code qui reste a etre tester et/ou modifier ***************************/

//Fonction de rapport des antennes
uint16_t reportDeploymentStatus(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b11000011);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet1= I2C_read_ack(I2C1);
	uint8_t octet2= I2C_read_nack(I2C1);
	I2C_stop(I2C1);

	return concatene(octet2,octet1);
}

//Fonctions de rapport du nombre d'activation
uint8_t reportAntenna_1_activationCount(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110000);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

uint8_t reportAntenna_2_activationCount(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110001);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

uint8_t reportAntenna_3_activationCount(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110010);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

uint8_t reportAntenna_4_activationCount(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110011);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

//Fonctions de rapport du temps d'activation
uint8_t reportAntenna_1_activationTime(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110100);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

uint8_t reportAntenna_2_activationTime(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110101);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

uint8_t reportAntenna_3_activationTime(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110110);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

uint8_t reportAntenna_4_activationTime(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b10110111);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	for (int i=0;i<100;i++)
		Console_Puts("F");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet= I2C_read_nack(I2C1);
	I2C_stop(I2C1);
	return octet;
}

//Fonction de mesure de temperature
uint16_t measureTemperature(uint8_t mc_adress){
	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Transmitter);
	I2C_write(I2C1,0b11000000);
	I2C_stop(I2C1);

	//Nous devons mettre un temps de decalage entre les commandes.
	Console_Puts("E\n");
	// ou
	//vTaskDelay(/*TBD*/);

	I2C_start(I2C1,mc_adress<<1,I2C_Direction_Receiver);
	uint8_t octet1= I2C_read_ack(I2C1);
	uint8_t octet2= I2C_read_nack(I2C1);
	I2C_stop(I2C1);

	return concatene(octet2,octet1);
}
