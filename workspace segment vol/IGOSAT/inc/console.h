/* IGOsat Console (Debug) and Command port implementation */
/* WARNING: Version Kit STM32F407G-DISC1 */

#define CONSOLE_TAKE(a)  xSemaphoreTake( SEMA_console ,portMAX_DELAY )
#define CONSOLE_GIVE(a)  xSemaphoreGive( SEMA_console );

extern SemaphoreHandle_t SEMA_console;  //  Semaphore to handle access to the Console/USART2 port

void Console_Init(void);
void Console_Putc(uint16_t c);
uint16_t Console_Getc(void);
void Console_Puts(const char *s);
void Console_Command(void*);
void Console_Init(void);
void usart2_init(void);

