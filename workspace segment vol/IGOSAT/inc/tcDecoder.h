/*
 * @titre : tcDedoder.h
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description :Ce fichier contient les fonctions permerttant d'interpreter les telecommandes recues. (C'est encore un code de test.)
 */
#include "configurationPanel.h"

//Tache lisant la FIFO des telecommandes recues
int vTC_decoder(void *);
