/*
 * @titre : bitset.h
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description :Ce fichier contient les fonctions permettant d'interpreter un tableau comme un ensemble de bit.
 */
#include <stdint.h>

//Structure permettant d'interpreter un tableau comme un ensemble de bit.
typedef struct{
	uint8_t bufferSize;

	uint8_t* buffer;
	uint8_t currentByte;

	uint8_t posInBuffer;
	uint8_t posInByte;
}BITSET;

//Initialise un ensemble de bit a partir d'un tableau.
void init_bitset(BITSET* bs, uint8_t*buf, int buffer_size);

//Retourne le prochain bit de l'ensemble. Ce bit retirer n'est plus dans l'ensemble.
uint8_t getNextBit(BITSET* bs);

//Retourne les prochains nbNextBits bit de l'ensemble. Ces bits retirer ne sont plus dans l'ensemble.
int getBits(BITSET* bs, int nbNextBits);
