/*
 * @titre : bitop.h
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description :Ce fichier contient les fonctions realisant des operations bit a bit.
 */
#include <stdint.h>

uint8_t getBit(int value, int pos);
uint16_t concatene(uint8_t d2,int8_t d1);
