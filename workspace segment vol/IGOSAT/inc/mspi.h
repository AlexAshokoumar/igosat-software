/* IGOsat SPI implementation */

#define SPI3_TAKE(a)  xSemaphoreTake( SEMA_SPI3 ,portMAX_DELAY )
#define SPI3_GIVE(a)  xSemaphoreGive( SEMA_SPI3 );

extern SemaphoreHandle_t SEMA_SPI3;  //  Semaphore to handle access to the SPI3 port

void spi3_init(void);
void Spi_Send(char c);
char Spi_Recv(void);
void Spi_get_flash_ID(void);
void Spi_get_flash_Status(void);
void Spi_set_flash_WE(void);
void Spi_reset_flash_WE(void);

