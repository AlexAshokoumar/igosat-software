/*
 * @titre : configurationPanel.h
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description : Representation du configuration panel.
 */
#include <stdint.h>

//Taille d'une configurationPanelTC en octet
#define NB_CPTC_BYTE 85

//Representation d'une configurationPanelTC
typedef struct {
	//SCI
	uint16_t Emin;
	uint16_t Emax;
	uint8_t Rsci;
	uint8_t Scale;
	//uint16_t V_pixel[16];
	uint8_t FT_convertisseur;
	uint8_t Tmin_convertisseur;
	uint8_t Tmax_convertisseur;
	uint16_t FT_SiPM;
	uint8_t Tmin_SiPM;
	uint8_t Tmax_SiPM;
	uint8_t FT_EASIROC;
	uint8_t Tmin_EASIROC;
	uint8_t Tmax_EASIROC;
	uint8_t FT_MC;
	uint8_t Tmin_MC;
	uint8_t Tmax_MC;
	uint8_t FV_LG_HG;
	uint8_t Vmin_LG_HG;
	uint8_t Vmax_LG_HG;
	uint8_t FV_HV;
	uint8_t Vmin_HV;
	uint8_t Vmax_HV;
	uint8_t FV_SiPM;
	uint8_t Vmin_SiPM;
	uint8_t Vmax_SiPM;

	//GPS
	uint8_t Ratio_scintillation;
	uint8_t Ratio_biais;
	uint8_t Acq_1;
	uint8_t Freq_echantillonage_TEC;
	uint8_t Freq_echantillonage_scintillation;
	uint8_t Freq_echantillonage_biais;
	//uint_t SNR_trigger;
	uint8_t Freq_Tel_TEC;
	uint8_t Freq_Tel_scintillation;
	uint8_t Freq_Tel_biais;
	//uint_t Vect_ang;
	uint8_t t_scintillation;
	//uint_t Nb_SNR;
	uint8_t Freq_Tel_positionning;
	uint8_t Acq_2;
	uint8_t FV_antenne;
	uint8_t Vmin_antenne;
	uint8_t Vmax_antenne;
	uint8_t FT_GPS;
	uint8_t Tmin_GPS;
	uint8_t Tmax_GPS;

	//SCAO
	uint8_t Kp;
	uint8_t Ki;
	uint8_t Kd;
	uint8_t Consign_SCA;
	uint8_t FI_bobine;
	uint8_t Imin_bobine;
	uint8_t Imax_bobine;
	uint8_t FT_bobine;
	uint8_t Tmin_bobine;
	uint8_t Tmax_bobine;
	uint8_t FT_H;
	uint8_t Tmin_H;
	uint8_t Tmax_H;
	uint16_t FT_magnet;
	uint8_t Tmin_magnet;
	uint8_t Tmax_magnet;
	uint8_t Angle;
	uint8_t FV_SCA;
	uint8_t Vmin_SCA;
	uint8_t Vmax_SCA;
	uint8_t Champ_F;
	//uint_t Champ_min;
	//uint_t Champ_max;

	//SAE
	uint8_t FT_solar;
	uint8_t Tmin_solar;
	uint8_t Tmax_solar;
	uint8_t FV_bat;
	uint8_t Vmin_bat;
	uint8_t Vmax_bat;
	uint8_t FC_bat;
	//uint_t Cmin_bat;
	//uint_t Cmax_bat;
	uint8_t Charge_bat;
	uint8_t FI_solar;
	uint8_t Imin_solar;
	uint8_t Imax_solar;
	uint16_t FI_C_D;
	uint8_t Imin_C_D;
	uint8_t Imax_C_D;
	uint8_t FV_rail;
	uint8_t Vmin_rail;
	uint8_t Vmax_rail;
	uint8_t FT_bat;
	uint8_t Tmin_bat;
	uint8_t Tmax_bat;
	uint8_t Bat_min;

	//ODB
	uint8_t FT_CPU;
	uint8_t Tmin_CPU;
	uint8_t Tmax_CPU;

	//TEL
	uint8_t FT_VHF_UHF;
	uint8_t Tmin_VHF_UHF;
	uint8_t Tmax_VHF_UHF;
	uint8_t FI_TX;
	uint8_t Imin_TX;
	uint8_t Imax_TX;
	uint8_t FT_Amp;
	uint8_t Tmin_Amp;
	uint8_t Tmax_Amp;
	uint8_t FP_Amp;
	//uint_t Pmin_Amp;
	//uint_t Pmax_Amp;
	uint8_t FP_TX;
	//uint_t Pmin_TX;
	//uint_t Pmax_TX;
	uint8_t NR;

	//STR
	uint8_t FT_mat;
	uint8_t Tmin_mat;
	uint8_t Tmax_mat;

} CONFIGURATION_PANEL;

//Valeur actuelle du configuration panel
CONFIGURATION_PANEL configPanel;
