/*
 * @titre : telecom.h
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description : Dans ce fichier se trouvent les fonctions permettant de communiquer avec la carte AMSAT-F.
 */
#define TRANSMITTER_TAKE(a)  xSemaphoreTake( transmitterMutex ,portMAX_DELAY )
#define TRANSMITTER_GIVE(a)  xSemaphoreGive( transmitterMutex );

#define RECEIVER_TAKE(a)  xSemaphoreTake( receiverMutex ,portMAX_DELAY )
#define RECEIVER_GIVE(a)  xSemaphoreGive( receiverMutex );

//GPIO
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"

//Interuption
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "misc.h"

//Queue
#include "FreeRTOS.h"
#include "queue.h"

//Semaphore
#include "semphr.h"

//AX 25
#include "ax25.h"

xSemaphoreHandle transmitterMutex;
xSemaphoreHandle receiverMutex;

xQueueHandle xAX25Queue;
#define TRANSMITTER_QUEUE_SIZE 200

xQueueHandle xReceiverQueue;
#define RECEIVER_QUEUE_SIZE 200

void init_telecom();
void send_data(uint8_t* buffer,uint8_t buffer_size);
