/*
 * @titre : reedSolomon.h
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description : Implementation du code correcteur d'erreur Reed Solomon. Ce code est adapter de http://rscode.sourceforge.net .
 */

/* Reed Solomon Coding for glyphs
 * Copyright Henry Minsky (hqm@alum.mit.edu) 1991-2009
 *
 * This software library is licensed under terms of the GNU GENERAL
 * PUBLIC LICENSE
 *
 * RSCODE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RSCODE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rscode.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Source code is available at http://rscode.sourceforge.net
 *
 * Commercial licensing is available under a separate license, please
 * contact author for details.
 *
 */

/****************************************************************
  
  Below is NPAR, the only compile-time parameter you should have to
  modify.
  
  It is the number of parity bytes which will be appended to
  your data to create a codeword.

  Note that the maximum codeword size is 255, so the
  sum of your message length plus parity should be less than
  or equal to this maximum limit.

  In practice, you will get slooow error correction and decoding
  if you use more than a reasonably small number of parity bytes.
  (say, 10 or 20)

  ****************************************************************/

#include <stdint.h>

#define NPAR 32
#define MAX_DATA_SIZE 255-NPAR

/*************************************/

/* Maximum degree of various polynomials. */
#define MAXDEG (NPAR*2)

/*************************************/
/* Encoder parity bytes */
int pBytes[MAXDEG];

/* generator polynomial */
int genPoly[MAXDEG*2];

/* Reed Solomon encode routines */
void initialize_ecc (void);
void encode_data (uint8_t msg[], int nbytes, uint8_t dst[]);


/* galois arithmetic tables */

/* This is one of 14 irreducible polynomials
 * of degree 8 and cycle length 255.
 * The high order 1 bit is implicit */
/* x^8 + x^4 + x^3 + x^2 + 1 */
#define PPOLY 0x1D 

int gexp[512];
int glog[256];

void init_galois_tables (void);
int ginv(int elt); 
int gmult(int a, int b);

/* polynomial arithmetic */
void mult_polys(int dst[], int p1[], int p2[]);
void copy_poly(int dst[], int src[]);
void zero_poly(int poly[]);
