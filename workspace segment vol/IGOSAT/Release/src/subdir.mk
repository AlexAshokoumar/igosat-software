################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/antenne.c \
../src/ax25.c \
../src/bitop.c \
../src/bitset.c \
../src/console.c \
../src/main.c \
../src/mi2c.c \
../src/mspi.c \
../src/newlib_stubs.c \
../src/reedSolomon.c \
../src/stm32f4_discovery.c \
../src/stm32f4xx_it.c \
../src/system_stm32f4xx.c \
../src/tcDecoder.c \
../src/telecom.c 

S_UPPER_SRCS += \
../src/startup_stm32f4xx.S 

O_SRCS += \
../src/main.o \
../src/startup_stm32f4xx.o \
../src/stm32f4_discovery.o \
../src/stm32f4xx_it.o \
../src/system_stm32f4xx.o 

OBJS += \
./src/antenne.o \
./src/ax25.o \
./src/bitop.o \
./src/bitset.o \
./src/console.o \
./src/main.o \
./src/mi2c.o \
./src/mspi.o \
./src/newlib_stubs.o \
./src/reedSolomon.o \
./src/startup_stm32f4xx.o \
./src/stm32f4_discovery.o \
./src/stm32f4xx_it.o \
./src/system_stm32f4xx.o \
./src/tcDecoder.o \
./src/telecom.o 

S_UPPER_DEPS += \
./src/startup_stm32f4xx.d 

C_DEPS += \
./src/antenne.d \
./src/ax25.d \
./src/bitop.d \
./src/bitset.d \
./src/console.d \
./src/main.d \
./src/mi2c.d \
./src/mspi.d \
./src/newlib_stubs.d \
./src/reedSolomon.d \
./src/stm32f4_discovery.d \
./src/stm32f4xx_it.d \
./src/system_stm32f4xx.d \
./src/tcDecoder.d \
./src/telecom.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DUSE_STDPERIPH_DRIVER -DSTM32F4XX -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wpadded -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g -DSTM32F407xx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -I"../inc" -I"../inc/FreeRTOS/Source/include" -I"../inc/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"../inc/STM32F4xx_StdPeriph_Driver" -I"../inc/CMSIS" -I"../inc/STM32F4xx" -std=gnu11 -Wmissing-prototypes -Wstrict-prototypes -Wbad-function-cast -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DUSE_STDPERIPH_DRIVER -DSTM32F4XX -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wpadded -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g -x assembler-with-cpp -DSTM32F407xx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -I"../inc" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


