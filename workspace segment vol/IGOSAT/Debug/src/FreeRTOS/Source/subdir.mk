################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/FreeRTOS/Source/croutine.c \
../src/FreeRTOS/Source/event_groups.c \
../src/FreeRTOS/Source/list.c \
../src/FreeRTOS/Source/queue.c \
../src/FreeRTOS/Source/tasks.c \
../src/FreeRTOS/Source/timers.c 

O_SRCS += \
../src/FreeRTOS/Source/croutine.o \
../src/FreeRTOS/Source/event_groups.o \
../src/FreeRTOS/Source/list.o \
../src/FreeRTOS/Source/queue.o \
../src/FreeRTOS/Source/tasks.o \
../src/FreeRTOS/Source/timers.o 

OBJS += \
./src/FreeRTOS/Source/croutine.o \
./src/FreeRTOS/Source/event_groups.o \
./src/FreeRTOS/Source/list.o \
./src/FreeRTOS/Source/queue.o \
./src/FreeRTOS/Source/tasks.o \
./src/FreeRTOS/Source/timers.o 

C_DEPS += \
./src/FreeRTOS/Source/croutine.d \
./src/FreeRTOS/Source/event_groups.d \
./src/FreeRTOS/Source/list.d \
./src/FreeRTOS/Source/queue.d \
./src/FreeRTOS/Source/tasks.d \
./src/FreeRTOS/Source/timers.d 


# Each subdirectory must supply rules for building sources it contributes
src/FreeRTOS/Source/%.o: ../src/FreeRTOS/Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DUSE_STDPERIPH_DRIVER -DSTM32F4XX -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wpadded -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -DDEBUG -DUSE_FULL_ASSERT -DSTM32F407xx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -I"C:\Users\toto\Desktop\igosat\STM32_Toolchain\workspace2\IGOSAT\inc" -I"C:\Users\toto\Desktop\igosat\STM32_Toolchain\workspace2\IGOSAT\inc\FreeRTOS\Source\include" -I"C:\Users\toto\Desktop\igosat\STM32_Toolchain\workspace2\IGOSAT\inc\FreeRTOS\Source\portable\GCC\ARM_CM4F" -I"C:\Users\toto\Desktop\igosat\STM32_Toolchain\workspace2\IGOSAT\inc\CMSIS" -I"C:\Users\toto\Desktop\igosat\STM32_Toolchain\workspace2\IGOSAT\inc\STM32F4xx" -I"C:\Users\toto\Desktop\igosat\STM32_Toolchain\workspace2\IGOSAT\inc\STM32F4xx_StdPeriph_Driver" -std=gnu11 -Wmissing-prototypes -Wstrict-prototypes -Wbad-function-cast -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


