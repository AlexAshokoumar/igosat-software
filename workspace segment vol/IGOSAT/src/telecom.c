/*
 * @titre : telecom.c
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description : Dans ce fichier se trouvent les fonctions permettant de communiquer avec la carte AMSAT-F.
 */
#include "telecom.h"

#include "bitset.h"
#include "bitop.h"
#include "reedSolomon.h"

void init_telecom_fifo();
void initTransmitter();

void Configure_PD0(void);
void EXTI0_IRQHandler(void);
uint8_t getNextBitWithBitStuffing();
void init_PA14();

init_UART3();
void USART3_IRQHandler(void);
uint16_t Telecom_Getc(void);

//Enumeration des differents champs de la trame AX 25
enum AX_25_FRAME_FIELD{
	  fanion, adresse, controle, pid, info, fcs, defaut
};

//Structure permettant l'envoi bit a bit des champs de la trame
typedef struct{
	TRAME_UI trame;

	enum AX_25_FRAME_FIELD currentField;
	uint8_t bytePosInCurrentField;

	uint8_t currentByte;
	uint8_t isCurrentByteEmpty;
	uint8_t isCurrentByteFlag;
	uint8_t bitPosInCurrentByte;

	uint8_t bitStuffNow;
	uint8_t nOnes;

}AX_25_FRAME_TRANSMITTER;

TRAME_UI trame;
AX_25_FRAME_TRANSMITTER transmitter;

uint8_t buffer[64];

//Initialisation du software de telecommunication
void init_telecom(){
	init_telecom_fifo();
	initialize_ecc();
	init_trame(&trame);
	initTransmitter();
	init_PA14();
	Configure_PD0();
	init_UART3();
}

//Initialisation des FIFO et leurs mutex
void init_telecom_fifo(){

	transmitterMutex=xSemaphoreCreateMutex();
	receiverMutex=xSemaphoreCreateMutex();

	xAX25Queue = xQueueCreate( TRANSMITTER_QUEUE_SIZE, sizeof(TRAME_UI) );
	xReceiverQueue = xQueueCreate( RECEIVER_QUEUE_SIZE, sizeof(uint8_t) );
	return;
}

//Initialisation de la structure permettant l'envoi bit a bit.
void initTransmitter(){
	transmitter.bytePosInCurrentField=0;
	transmitter.isCurrentByteEmpty=1;
	transmitter.bitPosInCurrentByte=0;
	transmitter.nOnes=0;
	transmitter.bitStuffNow=0;
}

//Entree du systeme permettant l'envoi du buffer passer en parametre
void send_data(uint8_t* data_buffer,uint8_t buffer_size){

	if(buffer_size<=MAX_DATA_SIZE){
		//uint8_t codeword_size=buffer_size+NPAR;
		//uint8_t codeword[codeword_size];
		//encode_data(data_buffer, buffer_size, codeword);
		set_info(&trame,data_buffer,buffer_size);
	}

	TRANSMITTER_TAKE();
	xQueueSendToBack(xAX25Queue,&trame,portMAX_DELAY);
	TRANSMITTER_GIVE();

}

//Configuration du pin PD0 qui recevra l'horloge.
/* Configure pins to be interrupts */
void Configure_PD0(void) {
    /* Set variables used */
    GPIO_InitTypeDef GPIO_InitStruct;
    EXTI_InitTypeDef EXTI_InitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;

    /* Enable clock for GPIOD */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    /* Enable clock for SYSCFG */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Set pin as input */
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOD, &GPIO_InitStruct);

    /* Tell system that you will use PD0 for EXTI_Line0 */
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource0);

    /* PD0 is connected to EXTI_Line0 */
    EXTI_InitStruct.EXTI_Line = EXTI_Line0;
    /* Enable interrupt */
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    /* Interrupt mode */
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    /* Triggers on rising and falling edge */
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
    /* Add to EXTI */
    EXTI_Init(&EXTI_InitStruct);

    /* Add IRQ vector to NVIC */
    /* PD0 is connected to EXTI_Line0, which has EXTI0_IRQn vector */
    NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
    /* Set priority */
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
    /* Set sub priority */
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
    /* Enable interrupt */
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    /* Add to NVIC */
    NVIC_Init(&NVIC_InitStruct);
}

//Handler de l'interruption
/* Set interrupt handlers */
/* Handle PD0 interrupt */
void EXTI0_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
        /* Do your stuff when PD0 is changed */

    	if(GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_0)==1){
    		if(getNextBitWithBitStuffing()==0){
    			GPIO_ToggleBits(GPIOA, GPIO_Pin_14);
    			//Console_Putc('0');
    		}
    		else{
    			//Console_Putc('1');
    		}
    	}

    	/* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}

//Configuration du pin qui enverra les donnees.
void init_PA14(){
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	//Initialize struct
	GPIO_InitTypeDef GPIO_InitDef;

	//Pins 14
	GPIO_InitDef.GPIO_Pin = GPIO_Pin_14;
	//Mode output
	GPIO_InitDef.GPIO_Mode = GPIO_Mode_OUT;
	//Output type push-pull
	GPIO_InitDef.GPIO_OType = GPIO_OType_PP;
	//Without pull resistors
	GPIO_InitDef.GPIO_PuPd = GPIO_PuPd_NOPULL;
	//50MHz pin speed
	GPIO_InitDef.GPIO_Speed = GPIO_Speed_50MHz;

	//Initialize pins on GPIOA port
	GPIO_Init(GPIOA, &GPIO_InitDef);
	/*
	GPIO_SetBits(GPIOA,GPIO_Pin_14);
	GPIO_ResetBits(GPIOA, GPIO_Pin_14);
	GPIO_ToggleBits(GPIOA, GPIO_Pin_14);
	*/
}

//Configuration du pin qui recevra les donnees de la carte AMSAT-F
init_UART3(){
	USART_InitTypeDef USART_InitStructure;

	GPIO_InitTypeDef GPIO_InitStructure;

	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx ;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC , ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	/* Connect PA2-3 to USARTx_Tx and USARTx_Rx*/
	//GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART3);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_USART3);

	/* Configure USART Tx as push-pull */
	//GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	//GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	//GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	//GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	//GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure USART Rx as input floating */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* USART configuration */
	USART_Init(USART3, &USART_InitStructure);

	/* Enable USART */
	USART_Cmd(USART3, ENABLE);

	/*Interruption
	NVIC_InitTypeDef NVIC_InitStruct;

	//Enable RX interrupt
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);

	// Set Channel to USART3
	// Set Channel Cmd to enable. That will enable USART3 channel in NVIC
	// Set Both priorities to 0. This means high priority

	// Initialize NVIC
	NVIC_InitStruct.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
	*/
}

//Handler de l'interruption
void USART3_IRQHandler(void) {
    //Check if interrupt was because data is received
    if (USART_GetITStatus(USART3, USART_IT_RXNE)) {
        //Do your stuff here

        //Clear interrupt flag
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
    }
}

//Lit la prochaine donnnee
uint16_t Telecom_Getc(void){
	while (USART_GetFlagStatus(USART3, USART_FLAG_RXNE) == RESET);
	return USART_ReceiveData(USART3);
}

//Envoi du prochain bit a envoyer en respectant le procotole AX 25
// TODO A optimiser
uint8_t getNextBitWithBitStuffing(){
	if(transmitter.bitStuffNow==1){
		transmitter.bitStuffNow=0;
		return 0;
	}
	if(transmitter.isCurrentByteEmpty==1){
		portBASE_TYPE xStatus = xQueueReceiveFromISR(xAX25Queue, &transmitter.trame, 0);

		if( xStatus != pdPASS ){
			transmitter.currentByte=FANION;
			transmitter.currentField=defaut;
		}
		else{
			transmitter.currentField=fanion;
			transmitter.bytePosInCurrentField=0;
			transmitter.currentByte=transmitter.trame.fanion;
		}
		transmitter.isCurrentByteFlag=1;
	}
	transmitter.isCurrentByteEmpty=0;
	uint8_t val=getBit(transmitter.currentByte,transmitter.bitPosInCurrentByte);
	transmitter.bitPosInCurrentByte++;

	//Bit Stuffing
	if(transmitter.isCurrentByteFlag==1){
		transmitter.nOnes=0;
	}
	else{
		if(val==1){
			transmitter.nOnes++;
		}
		else{
			transmitter.nOnes=0;
		}
	}

	if(transmitter.nOnes==5){
		transmitter.bitStuffNow=1;
	}

	if(transmitter.bitPosInCurrentByte==1){
		//Console_Putc('|');
	}

	if(transmitter.bitPosInCurrentByte>=8){

		switch(transmitter.currentField){
			case fanion:
				transmitter.currentField=adresse;
				transmitter.currentByte=transmitter.trame.adresse[0];
				transmitter.bytePosInCurrentField=1;
				transmitter.isCurrentByteFlag=0;
				break;
			case adresse:
				if(transmitter.bytePosInCurrentField<14){
					transmitter.currentByte=transmitter.trame.adresse[transmitter.bytePosInCurrentField];
					transmitter.bytePosInCurrentField++;
				}
				else{
					transmitter.currentField=controle;
					transmitter.currentByte=transmitter.trame.controle;
					transmitter.bytePosInCurrentField=1;
				}
				break;
			case controle:
				transmitter.currentField=pid;
				transmitter.currentByte=transmitter.trame.pid;
				transmitter.bytePosInCurrentField=1;
				break;
			case pid:
				if(transmitter.trame.info_size>0){
					transmitter.currentField=info;
					transmitter.currentByte=transmitter.trame.information[0];
					transmitter.bytePosInCurrentField=1;
				}
				else{
					transmitter.currentField=fcs;
					transmitter.currentByte=transmitter.trame.fcs;
					transmitter.bytePosInCurrentField=1;
				}
				break;
			case info:
				if(transmitter.bytePosInCurrentField<transmitter.trame.info_size){
					transmitter.currentByte=transmitter.trame.information[transmitter.bytePosInCurrentField];
					transmitter.bytePosInCurrentField++;
				}
				else{
					transmitter.currentField=fcs;
					transmitter.currentByte=transmitter.trame.fcs;
					transmitter.bytePosInCurrentField=1;
				}
				break;
			case fcs:
				if(transmitter.bytePosInCurrentField==1){
					transmitter.currentByte=transmitter.trame.fcs>>8;
					transmitter.bytePosInCurrentField++;
				}
				else{
					transmitter.isCurrentByteEmpty=1;
				}
				break;
			case defaut:
				transmitter.isCurrentByteEmpty=1;
				break;
			default:
				break;
		}
		transmitter.bitPosInCurrentByte=0;
	}
	return val;
}
