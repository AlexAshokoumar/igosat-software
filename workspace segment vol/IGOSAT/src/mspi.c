/* IGOsat SPI implementation */

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
//#include "timers.h"
#include "semphr.h"

#include <stdio.h>
#include <string.h>
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_spi.h"
#include "console.h"
#include "mspi.h"


SemaphoreHandle_t SEMA_SPI3;  //  Semaphore to handle access to the SPI3 port
char spicmd[8];  // buffer global declaration

void Spi_Send(char c)
{
	SPI_I2S_SendData(SPI3, (uint16_t) c);
	while (SPI_GetFlagStatus(SPI3, SPI_FLAG_TXE) == RESET);
}

char Spi_Recv(void)
{
	while (SPI_GetFlagStatus(SPI3, SPI_FLAG_RXNE) == RESET);
	return (char) SPI_I2S_ReceiveData(SPI3);
}

void spi3_init(void)  // Initializes SPI3 hardware and semaphore
{
    SPI_InitTypeDef SPI_InitStructure;
    //NVIC_InitTypeDef NVIC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    SPI_InitStructure.SPI_Direction= SPI_Direction_2Lines_FullDuplex ;
    SPI_InitStructure.SPI_Mode= SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize= SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL=SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA=SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_NSS=SPI_NSS_Soft;  /*!< Specifies whether the NSS signal is managed by
                                           hardware (NSS pin) or by software using the SSI bit.*/
    SPI_InitStructure.SPI_BaudRatePrescaler= SPI_BaudRatePrescaler_256;
    SPI_InitStructure.SPI_FirstBit= SPI_FirstBit_MSB;
    //SPI_InitStructure.SPI_CRCPolynomial= ;  /*!< Specifies the polynomial used for the CRC calculation. */

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA , ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB , ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);

    /* Connect PB3-4-5 to SPICK MISO MOSI */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_SPI3);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_SPI3);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_SPI3);

    /* Configure SPI_MOSI as push-pull */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Configure SPICK as push-pull */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Configure SPI_MISO as input floating */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    // Init PA7 to be FLASH_CS
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_SetBits(GPIOA, GPIO_Pin_7);
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_SetBits(GPIOA, GPIO_Pin_7);

    /* SPI1 configuration */
    SPI_Init(SPI3, &SPI_InitStructure);

    //SPI_ITConfig(SPI1, USART_IT_RXNE|USART_IT_TXE, ENABLE);
    //SPI_ITConfig(SPI1, USART_IT_RXNE, ENABLE);
    ///* Enable the SPI1 Interrupt */
	//NVIC_InitStructure.NVIC_IRQChannel = SPI1_IRQn;
	//NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	//NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	//NVIC_Init(&NVIC_InitStructure);

    /* Enable USART */
    SPI_Cmd(SPI3, ENABLE);

	SEMA_SPI3= xSemaphoreCreateBinary();  /* Create a semaphore for the SPI3 bus use */
	SPI3_GIVE(); /* release the SPI3 bus */

}

void Spi_get_flash_ID(void)
{
	char a,b,c;

	SPI3_TAKE(); /* Take the SPI3 bus */
	GPIO_ResetBits(GPIOA, GPIO_Pin_7); /* Activate the Flash SPI CS */
	Spi_Send((char) 0x9F);    // Send the Read ID command to the SPI Flash device
	a= Spi_Recv(); // dummy read

	Spi_Send(0x00);
	a= Spi_Recv();
	Spi_Send(0x00);
	b= Spi_Recv();
	Spi_Send(0x00);
	c= Spi_Recv();
	GPIO_SetBits(GPIOA, GPIO_Pin_7); /* Deactivate the Flash SPI CS */
	SPI3_GIVE(); /* release the SPI3 bus */

	CONSOLE_TAKE(); /* Take the Console semaphore */
	Console_Puts("\r\n");
	siprintf(spicmd,"%x",(unsigned char) a);
	Console_Puts("Spi_ID ");
	Console_Puts(spicmd);
	siprintf(spicmd," %x",(unsigned char) b);
	Console_Puts(spicmd);
	siprintf(spicmd," %x",(unsigned char) c);
	Console_Puts(spicmd);
	Console_Puts("\r\n");
	CONSOLE_GIVE(); /* release the Console semaphore */

}

void Spi_get_flash_Status(void)
{
	char a;

	SPI3_TAKE(); /* Take the SPI3 bus */
	GPIO_ResetBits(GPIOA, GPIO_Pin_7); /* Activate the Flash SPI CS */
	Spi_Send((char) 0x05);    // Send the Read Status command to the SPI Flash device
	a= Spi_Recv(); // dummy read
	Spi_Send(0x00);
	a= Spi_Recv();
	GPIO_SetBits(GPIOA, GPIO_Pin_7); /* Deactivate the Flash SPI CS */
	SPI3_GIVE(); /* release the SPI3 bus */

	CONSOLE_TAKE(); /* Take the Console semaphore */
	Console_Puts("\r\n");
	siprintf(spicmd,"%x",(unsigned char) a);
	Console_Puts("Spi_Status ");
	Console_Puts(spicmd);
	Console_Puts("\r\n");
	CONSOLE_GIVE(); /* release the Console semaphore */
}

void Spi_set_flash_WE(void)
{
	SPI3_TAKE(); /* Take the SPI3 bus */
	GPIO_ResetBits(GPIOA, GPIO_Pin_7); /* Activate the Flash SPI CS */
	Spi_Send((char) 0x06);    // Send the Write Enable command to the SPI Flash device
	Spi_Recv(); // dummy read
	GPIO_SetBits(GPIOA, GPIO_Pin_7); /* Deactivate the Flash SPI CS */
	SPI3_GIVE(); /* release the SPI3 bus */

	CONSOLE_TAKE(); /* Take the Console semaphore */
	Console_Puts("Flash Write Enabled\r\n");
	CONSOLE_GIVE(); /* release the Console semaphore */
}

void Spi_reset_flash_WE(void)
{
	SPI3_TAKE(); /* Take the SPI3 bus */
	GPIO_ResetBits(GPIOA, GPIO_Pin_7); /* Activate the Flash SPI CS */
	Spi_Send((char) 0x04);    // Send the Write Disable command to the SPI Flash device
	Spi_Recv(); // dummy read
	GPIO_SetBits(GPIOA, GPIO_Pin_7); /* Deactivate the Flash SPI CS */
	SPI3_GIVE(); /* release the SPI3 bus */

	CONSOLE_TAKE(); /* Take the Console semaphore */
	Console_Puts("Flash Write Disabled\r\n");
	CONSOLE_GIVE(); /* release the Console semaphore */
}

