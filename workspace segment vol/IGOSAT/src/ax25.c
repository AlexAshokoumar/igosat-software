/*
 * @titre : ax25.c
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description : Implementation du protocole AX 25
 */
#include <stdio.h>
#include <string.h>
#include "ax25.h"

//Initialisation des champs fanion, adresse, controle, PID et FCS
void init_trame(TRAME_UI *trame){
  trame->fanion=FANION;
  set_adresses(trame,DEST,SRC);
  trame->controle=TRAME_UI_CONTROLE;
  trame->pid=TRAME_UI_PID;
  trame->fcs=0x0000;
  return;
}

//Remplissage du champs info de la trame
int set_info(TRAME_UI* trame,uint8_t* info,uint8_t info_size){
  if(info_size>TAILLE_INFO_MAX){
	  return -1;
  }
  trame->information=info;
  trame->info_size=info_size;
  return 0;
}

//Remplissage du champs adresse
void set_adresses(TRAME_UI* trame,char dest[6],char src[6]){
  int pos=0;
  while(dest[pos]!='\0' && pos<6){
    trame->adresse[pos]=dest[pos];
    pos++;
  }
  for(;pos<6;pos++){
    trame->adresse[pos]=' ';
  }
  trame->adresse[6]=SSID;
  pos=0;
  while(src[pos]!='\0' && pos<13){
    trame->adresse[7+pos]=src[pos];
    pos++;
  }
  for(;pos<13;pos++){
    trame->adresse[7+pos]=' ';
  }
  trame->adresse[13]=SSID;

  return;
}


//TODO
/*
uint16_t calculFCS(char* buffer,unsigned short taille){
  unsigned int i,j;
  unsigned short shift_register=0xFFFF,outBit;
  char byte;

  for(i=0;i<taille;i++){
    byte=buffer[i];

    for(j=0;j<8;j++){
      outBit=shift_register & 0x0001;
      shift_register>>=0x01;

      if(outBit != (byte & 0x01)){
	shift_register ^=0x8408;
	byte>>=0x01;
      }

      byte>>=0x01;
    }
  }
  return shift_register ^ 0xFFFF;
}
*/

//CRC16-CCITT = x^16 + x^12 + x^5 + 1

uint16_t crchware(uint16_t data, uint16_t genpoly, uint16_t	 accum);

/* Computes the CRC-CCITT checksum on array of byte data, length len
*/
uint16_t calculFCS(uint8_t *msg, int len){
	int i;
	uint16_t acc = 0;

	for (i = 0; i < len; i++) {
		acc = crchware((uint16_t) msg[i], (uint16_t) 0x1021, acc);
	}

	return(acc);
}

/* models crc hardware (minor variation on polynomial division algorithm) */
uint16_t crchware(uint16_t data, uint16_t genpoly, uint16_t accum)
{
	static uint16_t i;
	data <<= 8;
	for (i = 8; i > 0; i--) {
		if ((data ^ accum) & 0x8000)
			accum = ((accum << 1) ^ genpoly) & 0xFFFF;
		else
			accum = (accum<<1) & 0xFFFF;
		data = (data<<1) & 0xFFFF;
	}
	return (accum);
}
