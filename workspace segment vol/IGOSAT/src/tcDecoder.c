/*
 * @titre : tcDedoder.h
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description :Ce fichier contient les fonctions permerttant d'interpreter les telecommandes recues. (C'est encore un code de test.)
 */
#include "tcDecoder.h"
#include "bitset.h"
#include "telecom.h"

void decodeTCheader(uint8_t header);
void decodeCPTC(uint8_t* buffer,int buffer_size,CONFIGURATION_PANEL* cp);

//Tache lisant la FIFO des telecommandes recues
int vTC_decoder(void* d){
	if(xReceiverQueue==NULL){
		return -1;
	}
	uint8_t lReceivedValue;

	portBASE_TYPE xStatus;

	char tab[64];

	while(1){
		xStatus = xQueueReceive( xReceiverQueue, &lReceivedValue, portMAX_DELAY );
		if( xStatus == pdPASS ){
			decodeTCheader(lReceivedValue);
		}
		else{
			Console_Puts( "Could not receive from the queue.\r\n" );
		}
	}
	return 0;
}

volatile int l=0;

//Analyse le header de la telecommande
void decodeTCheader(uint8_t header){
	uint8_t buffer[NB_CPTC_BYTE];
	switch(header){
	case 0x00:
		for(l=0;l<NB_CPTC_BYTE;l++){
			uint8_t lReceivedValue;
			xQueueReceive( xReceiverQueue, &lReceivedValue, portMAX_DELAY );
			buffer[l]=lReceivedValue;
		}
		decodeCPTC(buffer,NB_CPTC_BYTE,&configPanel);
		break;
	default:
		break;
	}
}

//Interprete une configurationPanelTC
void decodeCPTC(uint8_t* buffer,int buffer_size,CONFIGURATION_PANEL* cp) {

	BITSET dec;
	init_bitset(&dec, buffer,buffer_size);

	//SCI
	cp->Emin=getBits(&dec,11);
	cp->Emax=getBits(&dec,11);
	cp->Rsci=getBits(&dec,7);
	cp->Scale=getBits(&dec,1);
	//cp->V_pixel[16];
	cp->FT_convertisseur=getBits(&dec,7);
	cp->Tmin_convertisseur=getBits(&dec,8);
	cp->Tmax_convertisseur=getBits(&dec,8);
	cp->FT_SiPM=getBits(&dec,14);
	cp->Tmin_SiPM=getBits(&dec,8);
	cp->Tmax_SiPM=getBits(&dec,8);
	cp->FT_EASIROC=getBits(&dec,7);
	cp->Tmin_EASIROC=getBits(&dec,8);
	cp->Tmax_EASIROC=getBits(&dec,8);
	cp->FT_MC=getBits(&dec,7);
	cp->Tmin_MC=getBits(&dec,8);
	cp->Tmax_MC=getBits(&dec,8);
	cp->FV_LG_HG=getBits(&dec,7);
	cp->Vmin_LG_HG=getBits(&dec,6);
	cp->Vmax_LG_HG=getBits(&dec,6);
	cp->FV_HV=getBits(&dec,7);
	cp->Vmin_HV=getBits(&dec,6);
	cp->Vmax_HV=getBits(&dec,6);
	cp->FV_SiPM=getBits(&dec,7);
	cp->Vmin_SiPM=getBits(&dec,6);
	cp->Vmax_SiPM=getBits(&dec,6);

	//GPS
	cp->Ratio_scintillation=getBits(&dec,4);
	cp->Ratio_biais=getBits(&dec,4);
	cp->Acq_1=getBits(&dec,4);
	cp->Freq_echantillonage_TEC=getBits(&dec,4);
	cp->Freq_echantillonage_scintillation=getBits(&dec,5);
	cp->Freq_echantillonage_biais=getBits(&dec,4);
	//cp->SNR_trigger=getBits(&dec,);
	cp->Freq_Tel_TEC=getBits(&dec,4);
	cp->Freq_Tel_scintillation=getBits(&dec,5);
	cp->Freq_Tel_biais=getBits(&dec,4);
	//cp->Vect_ang=getBits(&dec,);
	cp->t_scintillation=getBits(&dec,7);
	//cp->Nb_SNR=getBits(&dec,);
	cp->Freq_Tel_positionning=getBits(&dec,7);
	cp->Acq_2=getBits(&dec,7);
	cp->FV_antenne=getBits(&dec,7);
	cp->Vmin_antenne=getBits(&dec,6);
	cp->Vmax_antenne=getBits(&dec,6);
	cp->FT_GPS=getBits(&dec,7);
	cp->Tmin_GPS=getBits(&dec,7);
	cp->Tmax_GPS=getBits(&dec,7);

	//SCAO
	cp->Kp=getBits(&dec,4);
	cp->Ki=getBits(&dec,4);
	cp->Kd=getBits(&dec,4);
	cp->Consign_SCA=getBits(&dec,6);
	cp->FI_bobine=getBits(&dec,7);
	cp->Imin_bobine=getBits(&dec,4);
	cp->Imax_bobine=getBits(&dec,4);
	cp->FT_bobine=getBits(&dec,7);
	cp->Tmin_bobine=getBits(&dec,7);
	cp->Tmax_bobine=getBits(&dec,7);
	cp->FT_H=getBits(&dec,7);
	cp->Tmin_H=getBits(&dec,7);
	cp->Tmax_H=getBits(&dec,7);
	cp->FT_magnet=getBits(&dec,14);
	cp->Tmin_magnet=getBits(&dec,7);
	cp->Tmax_magnet=getBits(&dec,7);
	cp->Angle=getBits(&dec,7);
	cp->FV_SCA=getBits(&dec,7);
	cp->Vmin_SCA=getBits(&dec,6);
	cp->Vmax_SCA=getBits(&dec,6);
	cp->Champ_F=getBits(&dec,7);
	//cp->Champ_min=getBits(&dec,);
	//cp->Champ_max=getBits(&dec,);

	//SAE
	cp->FT_solar=getBits(&dec,7);
	cp->Tmin_solar=getBits(&dec,7);
	cp->Tmax_solar=getBits(&dec,7);
	cp->FV_bat=getBits(&dec,7);
	cp->Vmin_bat=getBits(&dec,6);
	cp->Vmax_bat=getBits(&dec,6);
	cp->FC_bat=getBits(&dec,7);
	//cp->Cmin_bat=getBits(&dec,);
	//cp->Cmax_bat=getBits(&dec,);
	cp->Charge_bat=getBits(&dec,7);
	cp->FI_solar=getBits(&dec,7);
	cp->Imin_solar=getBits(&dec,4);
	cp->Imax_solar=getBits(&dec,4);
	cp->FI_C_D=getBits(&dec,14);
	cp->Imin_C_D=getBits(&dec,4);
	cp->Imax_C_D=getBits(&dec,4);
	cp->FV_rail=getBits(&dec,7);
	cp->Vmin_rail=getBits(&dec,6);
	cp->Vmax_rail=getBits(&dec,6);
	cp->FT_bat=getBits(&dec,7);
	cp->Tmin_bat=getBits(&dec,7);
	cp->Tmax_bat=getBits(&dec,7);
	cp->Bat_min=getBits(&dec,7);

	//ODB
	cp->FT_CPU=getBits(&dec,7);
	cp->Tmin_CPU=getBits(&dec,7);
	cp->Tmax_CPU=getBits(&dec,7);

	//TEL
	cp->FT_VHF_UHF=getBits(&dec,7);
	cp->Tmin_VHF_UHF=getBits(&dec,7);
	cp->Tmax_VHF_UHF=getBits(&dec,7);
	cp->FI_TX=getBits(&dec,7);
	cp->Imin_TX=getBits(&dec,4);
	cp->Imax_TX=getBits(&dec,4);
	cp->FT_Amp=getBits(&dec,7);
	cp->Tmin_Amp=getBits(&dec,7);
	cp->Tmax_Amp=getBits(&dec,7);
	cp->FP_Amp=getBits(&dec,7);
	//cp->Pmin_Amp=getBits(&dec,);
	//cp->Pmax_Amp=getBits(&dec,);
	cp->FP_TX=getBits(&dec,7);
	//cp->Pmin_TX=getBits(&dec,);
	//cp->Pmax_TX=getBits(&dec,);
	cp->NR=getBits(&dec,7);

	//STR
	cp->FT_mat=getBits(&dec,7);
	cp->Tmin_mat=getBits(&dec,7);
	cp->Tmax_mat=getBits(&dec,7);

	//Affichage du configuration panel
	char mess[256];

	siprintf(mess,"Emin 0x%X\r\n",cp->Emin);
	Console_Puts(mess);
	siprintf(mess,"Emax 0x%X\r\n",cp->Emax);
	Console_Puts(mess);
	siprintf(mess,"Rsci 0x%X\r\n",cp->Rsci);
	Console_Puts(mess);
	siprintf(mess,"Scale 0x%X\r\n",cp->Scale);
	Console_Puts(mess);
	siprintf(mess,"FT_convertisseur 0x%X\r\n",cp->FT_convertisseur);
	Console_Puts(mess);
	siprintf(mess,"Tmin_convertisseur 0x%X\r\n",cp->Tmin_convertisseur);
	Console_Puts(mess);
	siprintf(mess,"Tmax_convertisseur 0x%X\r\n",cp->Tmax_convertisseur);
	Console_Puts(mess);
	siprintf(mess,"FT_SiPM 0x%X\r\n",cp->FT_SiPM);
	Console_Puts(mess);
	siprintf(mess,"Tmin_SiPM 0x%X\r\n",cp->Tmin_SiPM);
	Console_Puts(mess);
	siprintf(mess,"Tmax_SiPM 0x%X\r\n",cp->Tmax_SiPM);
	Console_Puts(mess);
	siprintf(mess,"FT_EASIROC 0x%X\r\n",cp->FT_EASIROC);
	Console_Puts(mess);
	siprintf(mess,"Tmin_EASIROC 0x%X\r\n",cp->Tmin_EASIROC);
	Console_Puts(mess);
	siprintf(mess,"Tmax_EASIROC 0x%X\r\n",cp->Tmax_EASIROC);
	Console_Puts(mess);
	siprintf(mess, "FT_MC 0x%X\r\n", cp->FT_MC);
	Console_Puts(mess);
	siprintf(mess, "Tmin_MC 0x%X\r\n", cp->Tmin_MC);
	Console_Puts(mess);
	siprintf(mess, "Tmax_MC 0x%X\r\n", cp->Tmax_MC);
	Console_Puts(mess);
	siprintf(mess, "FV_LG_HG 0x%X\r\n", cp->FV_LG_HG);
	Console_Puts(mess);
	siprintf(mess, "Vmin_LG_HG 0x%X\r\n", cp->Vmin_LG_HG);
	Console_Puts(mess);
	siprintf(mess, "Vmax_LG_HG 0x%X\r\n", cp->Vmax_LG_HG);
	Console_Puts(mess);
	siprintf(mess, "FV_HV 0x%X\r\n", cp->FV_HV);
	Console_Puts(mess);
	siprintf(mess, "Vmin_HV 0x%X\r\n", cp->Vmin_HV);
	Console_Puts(mess);
	siprintf(mess, "Vmax_HV 0x%X\r\n", cp->Vmax_HV);
	Console_Puts(mess);
	siprintf(mess, "FV_SiPM 0x%X\r\n", cp->FV_SiPM);
	Console_Puts(mess);
	siprintf(mess, "Vmin_SiPM 0x%X\r\n", cp->Vmin_SiPM);
	Console_Puts(mess);
	siprintf(mess, "Vmax_SiPM 0x%X\r\n", cp->Vmax_SiPM);
	Console_Puts(mess);

	//GPS
	siprintf(mess, "Ratio_scintillation 0x%X\r\n", cp->Ratio_scintillation);
	Console_Puts(mess);
	siprintf(mess, "Ratio_biais 0x%X\r\n", cp->Ratio_biais);
	Console_Puts(mess);
	siprintf(mess, "Acq_1 0x%X\r\n", cp->Acq_1);
	Console_Puts(mess);
	siprintf(mess, "Freq_echantillonage_TEC 0x%X\r\n",cp->Freq_echantillonage_TEC);
	Console_Puts(mess);
	siprintf(mess, "Freq_echantillonage_scintillation 0x%X\r\n",cp->Freq_echantillonage_scintillation);
	Console_Puts(mess);
	siprintf(mess, "Freq_echantillonage_biais 0x%X\r\n",cp->Freq_echantillonage_biais);
	Console_Puts(mess);
	//uint_t SNR_trigger;
	siprintf(mess, "Freq_Tel_TEC 0x%X\r\n", cp->Freq_Tel_TEC);
	Console_Puts(mess);
	siprintf(mess, "Freq_Tel_scintillation 0x%X\r\n",cp->Freq_Tel_scintillation);
	Console_Puts(mess);
	siprintf(mess, "Freq_Tel_biais 0x%X\r\n", cp->Freq_Tel_biais);
	Console_Puts(mess);
	//uint_t Vect_ang;
	siprintf(mess, "t_scintillation 0x%X\r\n", cp->t_scintillation);
	Console_Puts(mess);
	//uint_t Nb_SNR;
	siprintf(mess, "Freq_Tel_positionning 0x%X\r\n", cp->Freq_Tel_positionning);
	Console_Puts(mess);
	siprintf(mess, "Acq_2 0x%X\r\n", cp->Acq_2);
	Console_Puts(mess);
	siprintf(mess, "FV_antenne 0x%X\r\n", cp->FV_antenne);
	Console_Puts(mess);
	siprintf(mess, "Vmin_antenne 0x%X\r\n", cp->Vmin_antenne);
	Console_Puts(mess);
	siprintf(mess, "Vmax_antenne 0x%X\r\n", cp->Vmax_antenne);
	Console_Puts(mess);
	siprintf(mess, "FT_GPS 0x%X\r\n", cp->FT_GPS);
	Console_Puts(mess);
	siprintf(mess, "Tmin_GPS 0x%X\r\n", cp->Tmin_GPS);
	Console_Puts(mess);
	siprintf(mess, "Tmax_GPS 0x%X\r\n", cp->Tmax_GPS);
	Console_Puts(mess);

	//SCAO
	siprintf(mess, "Kp 0x%X\r\n", cp->Kp);
	Console_Puts(mess);
	siprintf(mess, "Ki 0x%X\r\n", cp->Ki);
	Console_Puts(mess);
	siprintf(mess, "Kd 0x%X\r\n", cp->Kd);
	Console_Puts(mess);
	siprintf(mess, "Consign_SCA 0x%X\r\n", cp->Consign_SCA);
	Console_Puts(mess);
	siprintf(mess, "FI_bobine 0x%X\r\n", cp->FI_bobine);
	Console_Puts(mess);
	siprintf(mess, "Imin_bobine 0x%X\r\n", cp->Imin_bobine);
	Console_Puts(mess);
	siprintf(mess, "Imax_bobine 0x%X\r\n", cp->Imax_bobine);
	Console_Puts(mess);
	siprintf(mess, "FT_bobine 0x%X\r\n", cp->FT_bobine);
	Console_Puts(mess);
	siprintf(mess, "Tmin_bobine 0x%X\r\n", cp->Tmin_bobine);
	Console_Puts(mess);
	siprintf(mess, "Tmax_bobine 0x%X\r\n", cp->Tmax_bobine);
	Console_Puts(mess);
	siprintf(mess, "FT_H 0x%X\r\n", cp->FT_H);
	Console_Puts(mess);
	siprintf(mess, "Tmin_H 0x%X\r\n", cp->Tmin_H);
	Console_Puts(mess);
	siprintf(mess, "Tmax_H 0x%X\r\n", cp->Tmax_H);
	Console_Puts(mess);
	siprintf(mess, "FT_magnet 0x%X\r\n", cp->FT_magnet);
	Console_Puts(mess);
	siprintf(mess, "Tmin_magnet 0x%X\r\n", cp->Tmin_magnet);
	Console_Puts(mess);
	siprintf(mess, "Tmax_magnet 0x%X\r\n", cp->Tmax_magnet);
	Console_Puts(mess);
	siprintf(mess, "Angle 0x%X\r\n", cp->Angle);
	Console_Puts(mess);
	siprintf(mess, "FV_SCA 0x%X\r\n", cp->FV_SCA);
	Console_Puts(mess);
	siprintf(mess, "Vmin_SCA 0x%X\r\n", cp->Vmin_SCA);
	Console_Puts(mess);
	siprintf(mess, "Vmax_SCA 0x%X\r\n", cp->Vmax_SCA);
	Console_Puts(mess);
	siprintf(mess, "Champ_F 0x%X\r\n", cp->Champ_F);
	Console_Puts(mess);
	//uint_t Champ_min;
	//uint_t Champ_max;

	//SAE
	siprintf(mess, "FT_solar 0x%X\r\n", cp->FT_solar);
	Console_Puts(mess);
	siprintf(mess, "Tmin_solar 0x%X\r\n", cp->Tmin_solar);
	Console_Puts(mess);
	siprintf(mess, "Tmax_solar 0x%X\r\n", cp->Tmax_solar);
	Console_Puts(mess);
	siprintf(mess, "FV_bat 0x%X\r\n", cp->FV_bat);
	Console_Puts(mess);
	siprintf(mess, "Vmin_bat 0x%X\r\n", cp->Vmin_bat);
	Console_Puts(mess);
	siprintf(mess, "Vmax_bat 0x%X\r\n", cp->Vmax_bat);
	Console_Puts(mess);
	siprintf(mess, "FC_bat 0x%X\r\n", cp->FC_bat);
	Console_Puts(mess);
	//uint_t Cmin_bat;
	//uint_t Cmax_bat;
	siprintf(mess, "Charge_bat 0x%X\r\n", cp->Charge_bat);
	Console_Puts(mess);
	siprintf(mess, "FI_solar 0x%X\r\n", cp->FI_solar);
	Console_Puts(mess);
	siprintf(mess, "Imin_solar 0x%X\r\n", cp->Imin_solar);
	Console_Puts(mess);
	siprintf(mess, "Imax_solar 0x%X\r\n", cp->Imax_solar);
	Console_Puts(mess);
	siprintf(mess, "FI_C_D 0x%X\r\n", cp->FI_C_D);
	Console_Puts(mess);
	siprintf(mess, "Imin_C_D 0x%X\r\n", cp->Imin_C_D);
	Console_Puts(mess);
	siprintf(mess, "Imax_C_D 0x%X\r\n", cp->Imax_C_D);
	Console_Puts(mess);
	siprintf(mess, "FV_rail 0x%X\r\n", cp->FV_rail);
	Console_Puts(mess);
	siprintf(mess, "Vmin_rail 0x%X\r\n", cp->Vmin_rail);
	Console_Puts(mess);
	siprintf(mess, "Vmax_rail 0x%X\r\n", cp->Vmax_rail);
	Console_Puts(mess);
	siprintf(mess, "FT_bat 0x%X\r\n", cp->FT_bat);
	Console_Puts(mess);
	siprintf(mess, "Tmin_bat 0x%X\r\n", cp->Tmin_bat);
	Console_Puts(mess);
	siprintf(mess, "Tmax_bat 0x%X\r\n", cp->Tmax_bat);
	Console_Puts(mess);
	siprintf(mess, "Bat_min 0x%X\r\n", cp->Bat_min);
	Console_Puts(mess);

	//ODB
	siprintf(mess, "FT_CPU 0x%X\r\n", cp->FT_CPU);
	Console_Puts(mess);
	siprintf(mess, "Tmin_CPU 0x%X\r\n", cp->Tmin_CPU);
	Console_Puts(mess);
	siprintf(mess, "Tmax_CPU 0x%X\r\n", cp->Tmax_CPU);
	Console_Puts(mess);

	//TEL
	siprintf(mess, "FT_VHF_UHF 0x%X\r\n", cp->FT_VHF_UHF);
	Console_Puts(mess);
	siprintf(mess, "Tmin_VHF_UHF 0x%X\r\n", cp->Tmin_VHF_UHF);
	Console_Puts(mess);
	siprintf(mess, "Tmax_VHF_UHF 0x%X\r\n", cp->Tmax_VHF_UHF);
	Console_Puts(mess);
	siprintf(mess, "FI_TX 0x%X\r\n", cp->FI_TX);
	Console_Puts(mess);
	siprintf(mess, "Imin_TX 0x%X\r\n", cp->Imin_TX);
	Console_Puts(mess);
	siprintf(mess, "Imax_TX 0x%X\r\n", cp->Imax_TX);
	Console_Puts(mess);
	siprintf(mess, "FT_Amp 0x%X\r\n", cp->FT_Amp);
	Console_Puts(mess);
	siprintf(mess, "Tmin_Amp 0x%X\r\n", cp->Tmin_Amp);
	Console_Puts(mess);
	siprintf(mess, "Tmax_Amp 0x%X\r\n", cp->Tmax_Amp);
	Console_Puts(mess);
	siprintf(mess, "FP_Amp 0x%X\r\n", cp->FP_Amp);
	Console_Puts(mess);
	//uint_t Pmin_Amp;
	//uint_t Pmax_Amp;
	siprintf(mess, "FP_TX 0x%X\r\n", cp->FP_TX);
	Console_Puts(mess);
	//uint_t Pmin_TX;
	//uint_t Pmax_TX;
	siprintf(mess, "NR 0x%X\r\n", cp->NR);
	Console_Puts(mess);

	//STR
	siprintf(mess, "FT_mat 0x%X\r\n", cp->FT_mat);
	Console_Puts(mess);
	siprintf(mess, "Tmin_mat 0x%X\r\n", cp->Tmin_mat);
	Console_Puts(mess);
	siprintf(mess, "Tmax_t 0x%X\r\n", cp->Tmax_mat);
	Console_Puts(mess);

	return;
}

