/* IGOsat Console (Debug) and Command port implementation */
/* WARNING: Version Kit STM32F407G-DISC1 */

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
//#include "timers.h"
#include "semphr.h"

#include <stdio.h>
#include <string.h>
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_usart.h"
#include "console.h"

extern   uint32_t SystemCoreClock;

//StaticSemaphore_t mem_sema_console;
SemaphoreHandle_t SEMA_console;  //  Semaphore to handle access to the Console/USART2 port

char cstr64[64];  // global declaration of the 64 char string used for Console Command
TaskStatus_t pxTaskStatusArray[31];  // buffer for the ps command


void Console_Putc(uint16_t c)
{
	USART_SendData(USART2, c & 0xFF);
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
}

uint16_t Console_Getc(void)
{
	while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET);
	return USART_ReceiveData(USART2);
}

void Console_Puts(const char *s)
{
	uint16_t c;
	while (*s != '\0') { c= (uint16_t) *s++ ; Console_Putc(c); }
}

void cmd_ps(void);
void cmd_ps(void)
{
	uint16_t i;
	//char buff[512];
	/* Take the Console semaphore */
	CONSOLE_TAKE();
	Console_Puts("ps\r\n");

	i = (uint16_t) uxTaskGetNumberOfTasks();
	siprintf(cstr64,"Found %i tasks \r\n",i);

	Console_Puts(cstr64);
	Console_Puts(" Id  St  Pri Name\r\n");
	Console_Puts("\r\n");


	for (i=0;i<uxTaskGetSystemState(pxTaskStatusArray,31,NULL);i++)
	{
		siprintf(cstr64," %2i  %2i  %2i  ",(int) pxTaskStatusArray[i].xTaskNumber,
				(int)pxTaskStatusArray[i].eCurrentState,(int)pxTaskStatusArray[i].uxBasePriority);
		Console_Puts(cstr64);
		Console_Puts(pxTaskStatusArray[i].pcTaskName);

		Console_Puts("\r\n");
	}
	CONSOLE_GIVE(); /* release the Console semaphore */
}


void cmd_ck(void);
void cmd_ck(void)
{
	        CONSOLE_TAKE(); /* Take the Console semaphore */
	        Console_Puts("ck\r\n");
	        siprintf(cstr64,"REGISTER = %08x SysClk = %i",RCC->CR,SystemCoreClock);
	        Console_Puts(cstr64);
	        Console_Puts("\r\n");
	        CONSOLE_GIVE(); /* release the Console semaphore */
}


// TASK COMMAND: SHELL-like command task for the IGOsat Console
void Console_Command(void *pvParameters)
{
	uint8_t i;
	CONSOLE_TAKE(); /* Take the Console semaphore */
	Console_Puts("Starting Command Console\r\n");
	CONSOLE_GIVE();   /* release the Console semaphore */
	while (1)
	{
		CONSOLE_TAKE(); /* Take the Console semaphore */
		Console_Puts("> ");
		CONSOLE_GIVE();   /* release the Console semaphore */
		cstr64[0]= 0;
		for(i=0;i<64;i++)
		{
			cstr64[i]= (char) (Console_Getc() & 0x00FF );
			if (cstr64[i]==(char) 13) break; /* quit the for, if CR detected */
		}
		//iprintf("%i\r\n",cstr64[2]);
		cstr64[i]=0;
		if (i==0)
		{
			CONSOLE_TAKE(); /* Take the Console semaphore */
			Console_Puts("\r\n");
			CONSOLE_GIVE();   /* release the Console semaphore */
			continue;
		}
		if (strncmp("ps",cstr64,i)==0) { cmd_ps(); continue; }
		if (strncmp("ck",cstr64,i)==0) { cmd_ck(); continue; }

		Console_Puts("\r\n");
		Console_Puts("UNKNOWN: ");
		Console_Puts(cstr64);
		Console_Puts("\r\n");
	}
	Console_Puts("QUITTING Console \r\n");
}

void Console_Init(void)
{
	SEMA_console= xSemaphoreCreateBinary();  /* Create a semaphore for the USART/Console use */
	USART2_init();     /* Initialize USART port for Console */
	xTaskCreate(       /* Create tasks */
		  Console_Command,          /* Function pointer */
		  "ICMD",                   /* Task name */
		  configMINIMAL_STACK_SIZE, /* Stack depth in words */
		  (void*) NULL,             /* Pointer to arguments */
		  tskIDLE_PRIORITY + 2UL,   /* Task priority */
		  NULL                      /* Task handle */
 	);

	Console_Puts("Console_Init done\r\n");
	CONSOLE_GIVE(); /* release the Console semaphore */
}

void USART2_init(void)
{
    USART_InitTypeDef USART_InitStructure;
    //NVIC_InitTypeDef NVIC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    USART_InitStructure.USART_BaudRate = 9600;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA , ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

    /* Connect PA2-3 to USARTx_Tx and USARTx_Rx*/
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

    /* Configure USART Tx as push-pull */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Configure USART Rx as input floating */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* USART configuration */
    USART_Init(USART2, &USART_InitStructure);

    //USART_ITConfig(USART2, USART_IT_RXNE|USART_IT_TXE, ENABLE);
    //USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
    /* Enable the USART2 Interrupt */
	//NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	//NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	//NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	//NVIC_Init(&NVIC_InitStructure);

    /* Enable USART */
    USART_Cmd(USART2, ENABLE);

}
