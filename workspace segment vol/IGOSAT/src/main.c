#include <stm32f4xx.h>

#include <stm32f4xx_usart.h> // under Libraries/STM32F4xx_StdPeriph_Driver/inc and src

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "croutine.h"
#include "console.h"
#include "mspi.h"
#include "mi2c.h"
#include "reedSolomon.h"

void vPrintUart(void* d);

void vPrintUart(void* d)
{
 portTickType last_wakeup_time;
 last_wakeup_time = xTaskGetTickCount();

 while(1){
	 CONSOLE_TAKE(); //take the Console semaphore
	 Console_Puts("Comm_Serie_IgoSAT \r\n");
	 CONSOLE_GIVE(); //release the Console semaphore
	 vTaskDelayUntil(&last_wakeup_time, 1500/portTICK_RATE_MS);
	}
}

#include "telecom.h"

volatile int i=0;

int main(){

	Console_Init();
	//SystemInit();

	init_telecom();
	//while (1) Console_Putc(Telecom_Getc());

	//uint8_t info[]="A\r\n";
	uint8_t info1[]="Bonjour Bernard\r\n";
	uint8_t info2[]="Bonjour Bernard\r\n";
	uint8_t info3[]="Bonjour Bernard\r\n";
	uint8_t info4[]="Bonjour Bernard\r\n";
	uint8_t info5[]="Bonjour Bernard\r\n";
	uint8_t info6[]="Bonjour Bernard\r\n";
	uint8_t info7[]="Bonjour Bernard\r\n";
	uint8_t info8[]="Bonjour Bernard\r\n";
	uint8_t info9[]="Bonjour Bernard\r\n";
	uint8_t info10[]="Bonjour Bernard\r\n";

	for(i=0;i<100;i++){
		Console_Puts("Attente\r\n");
	}
	//send_data(info,sizeof(info));
	send_data(info1,sizeof(info1));
	send_data(info2,sizeof(info2));
	send_data(info3,sizeof(info3));
	send_data(info4,sizeof(info4));
	send_data(info5,sizeof(info5));
	send_data(info6,sizeof(info6));
	send_data(info7,sizeof(info7));
	send_data(info8,sizeof(info8));
	send_data(info9,sizeof(info9));
	send_data(info10,sizeof(info10));

	while (1) {
		Console_Puts("Waiting for interrupt\r\n");
	}
}

/*
int main(void) {
  //void initHW();
  Console_Init();
  spi3_init();
  //initialize_ecc ();

  xTaskCreate(vPrintUart, "Uart",configMINIMAL_STACK_SIZE,NULL,tskIDLE_PRIORITY + 2UL,NULL);
  vTaskStartScheduler();
  while (1);
  return 0;
}
*/
