/*
 * @titre : bitop.c
 * @Auteur: Alex Ashokoumar
 * @mail : alex.ashokoumar@hotmail.fr
 *
 * @description :Ce fichier contient les fonctions realisant des operations bit a bit.
 */
#include "bitop.h"

uint8_t getBit(int value, int pos) {
	return (value >> pos) & 1;
}

uint16_t concatene(uint8_t d2,int8_t d1){
	return ((uint16_t)d2<<8) |d1;
}
