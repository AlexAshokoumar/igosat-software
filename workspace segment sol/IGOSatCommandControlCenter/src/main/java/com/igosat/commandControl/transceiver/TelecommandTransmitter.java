package com.igosat.commandControl.transceiver;

import java.io.IOException;
import java.io.OutputStream;

import com.igosat.commandControl.telecommand.Telecommand;

/**
 * @author Alex Ashokoumar
 *
 */
public class TelecommandTransmitter extends Transmitter{

	public TelecommandTransmitter(OutputStream output) {
		super(output);
	}
	
	public void send(Telecommand tc) throws IOException{
		super.send(tc.getBytes());
	}

}
