package com.igosat.commandControl.telecommand;

import java.util.BitSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.java.manip.IntegerManipulation;
import com.java.manip.StringManipulation;

/**
 * @author Alex Ashokoumar
 *
 */
public class Telecommand {
	
	private BitSet bitSet=new BitSet();
	private int index=0;
	
	private static Logger logger = LogManager.getLogger(Telecommand.class);
	
	public void addInt(int value,int nbBit){
		
		if(nbBit>=Integer.SIZE){
			//TODO Should throw Exception
		}
		if(value>=Math.pow(2,nbBit)){
			//TODO Should throw Exception
		}
		
		for(int i=0;i<nbBit;i++){
			if(IntegerManipulation.getBit(value,i)==1){
				this.bitSet.set(this.index+i);
			}
		}
		index+=nbBit;
	}
	
	@Override
	public String toString(){
		return StringManipulation.toBinaryString(this.getBytes());
	}
	
	public byte[] getBytes(){
		byte[]buffer=this.bitSet.toByteArray();
		if(this.index<=buffer.length*8){
			return buffer;
		}
		byte[]buf=new byte[buffer.length+1];
		for(int i=0;i<buffer.length;i++){
			buf[i]=buffer[i];
		}
		buf[buffer.length]=0x00;
		return buf;
	}
	
}