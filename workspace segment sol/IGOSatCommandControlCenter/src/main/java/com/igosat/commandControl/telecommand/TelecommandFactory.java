package com.igosat.commandControl.telecommand;

import com.igosat.configurationPanel.ConfigurationPanelParam;
import com.igosat.configurationPanel.dao.ConfigurationDAOFactory;
import com.igosat.configurationPanel.dao.ConfigurationPanelDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class TelecommandFactory {
	
	public static Telecommand createConfigurationPanelTC(ConfigurationPanelDAO cp){
		Telecommand tc=new Telecommand();
		ConfigurationPanelParam[]cpTab=ConfigurationPanelParam.values();
		
		tc.addInt(TelecommandConfiguration.ConfigurationPanelTC.getID(), 8);
		for(ConfigurationPanelParam param : cpTab){
			tc.addInt(cp.getIntByConfigurationPanelParam(param), param.getNbBit());
		}
		
		return tc;
	}
	
	public static void main(String[]args){
		Telecommand tc=TelecommandFactory.createConfigurationPanelTC(ConfigurationDAOFactory.createConfigurationPanelDAO());
		String s=tc.toString();
		s=s.replaceAll(" ",", 0b");
		System.out.println(s);
	}
	
}
