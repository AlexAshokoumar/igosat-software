package com.igosat.commandControl.transceiver;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Alex Ashokoumar
 *
 */
public class Transmitter {
	
	public static final int maxMessageLength=192;

	private OutputStream output;
	
	public Transmitter(OutputStream output){
		this.output=output;
	}
	
	public void send(byte[]message) throws IOException{
		if(message.length>this.maxMessageLength){
			//TODO Should throw an Exception
		}
		this.output.write(message);
	}
	
}
