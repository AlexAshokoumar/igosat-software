package com.igosat.commandControl.telecommand;

/**
 * @author Alex Ashokoumar
 *
 */
public enum TelecommandConfiguration {
	
	ConfigurationPanelTC("ConfigurationPanelTC",(byte)0x00),
	PingTC("PingTC",(byte)0x01),
	DumpTC("DumpTC",(byte)0x02);
	
	private String name;
	private byte id;

	TelecommandConfiguration(String name, byte id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public byte getID() {
		return this.id;
	}

	public String toString() {
		return name;
	}

}
