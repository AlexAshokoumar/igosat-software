package com.igosat.configurationPanel.dao;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.fromXML.ConfigurationPanelDAOImplXML;

/**
 * @author Alex Ashokoumar
 *
 */
public class ConfigurationDAOFactory {
	
	public static String defaultXMLFilePath="src/main/resources/configurationPanel.xml";
	
	public static ConfigurationPanelDAO createConfigurationPanelDAO(){
		ConfigurationPanelDAO config=null;
		try {
			config= new ConfigurationPanelDAOImplXML(defaultXMLFilePath);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return config;
	}
	
}
