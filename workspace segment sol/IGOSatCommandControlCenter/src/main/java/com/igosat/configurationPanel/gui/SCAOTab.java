package com.igosat.configurationPanel.gui;

import com.igosat.configurationPanel.ConfigurationPanelParam;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author Alex Ashokoumar
 *
 */
public class SCAOTab extends CPTab {
	
	public TitledPane SCAO_Pane=new TitledPane("SCAO",new GridPane());//(new GridLayout(23,2))

	public SCAOTab(String title) {
		super(title);
		
		GridPane gridPane;
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		
		int rowIndex=0;
		
		gridPane=(GridPane)this.SCAO_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Kp.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Ki.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Kd.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imin_bobine.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imax_bobine.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_bobine.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_bobine.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_bobine.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_H.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_H.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_H.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_magnet.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_magnet.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_magnet.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Angle.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FV_SCA.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmin_SCA.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmax_SCA.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Champ_F.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Champ_min.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Champ_max.getName()), new TextField(),rowIndex++);
		
		this.setContent(this.SCAO_Pane);
	}

}
