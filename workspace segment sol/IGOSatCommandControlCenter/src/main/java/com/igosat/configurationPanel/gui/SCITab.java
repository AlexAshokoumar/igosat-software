package com.igosat.configurationPanel.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.igosat.configurationPanel.ConfigurationPanelParam;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * @author Alex Ashokoumar
 *
 */
public class SCITab extends CPTab{
	
	public TitledPane Energie_Pane=new TitledPane("Energie",new GridPane());//new GridPane(2,2)
	public TitledPane Resolution_Pane=new TitledPane("Resolution",new GridPane());//(new GridLayout(1,2));
	public TitledPane Echelle_Pane=new TitledPane("Echelle",new GridPane());//(new GridLayout(1,2));
	public TitledPane Pixel_Pane=new TitledPane("Pixel",new GridPane());//(new GridLayout(1,2));
	public TitledPane Convertisseur_Pane=new TitledPane("Convertisseur",new GridPane());//(new GridLayout(3,2));
	public TitledPane SiPM_Pane=new TitledPane("SiPM",new GridPane());//(new GridLayout(6,2));
	public TitledPane EASIROC_Pane=new TitledPane("EASIROC",new GridPane());//(new GridLayout(3,2));
	public TitledPane MC_Pane=new TitledPane("MC",new GridPane());//(new GridLayout(3,2));
	public TitledPane LG_HG_Pane=new TitledPane("LG/HG",new GridPane());//(new GridLayout(3,2));
	public TitledPane HV_Pane=new TitledPane("HV",new GridPane());//(new GridLayout(3,2));
	
	private static Logger logger = LogManager.getLogger(SCITab.class);
	
	public SCITab(String title) {
		super(title);
		this.initPanes();
		this.organizePanelIntoTab();
	}
	
	private void initPanes(){
		GridPane gridPane;
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		
		int rowIndex=0;
		
		gridPane=(GridPane)this.Energie_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Emin.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Emax.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.Resolution_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Rsci.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.Echelle_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Scale.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.Pixel_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label("V_pixel"), new Button("Change"),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.Convertisseur_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_convertisseur.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_convertisseur.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_convertisseur.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.SiPM_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_SiPM.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_SiPM.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_SiPM.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FV_SiPM.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmin_SiPM.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmax_SiPM.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.EASIROC_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_EASIROC.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_EASIROC.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_EASIROC.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.MC_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_MC.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_MC.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_MC.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.LG_HG_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FV_LG_HG.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmin_LG_HG.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmax_LG_HG.getName()), new TextField(),rowIndex++);
		
		rowIndex=0;
		
		gridPane=(GridPane)this.HV_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FV_HV.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmin_HV.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmax_HV.getName()), new TextField(),rowIndex++);
	}
	
	private void organizePanelIntoTab(){
		
		VBox vBox1= new VBox(this.Pixel_Pane,this.Convertisseur_Pane,this.SiPM_Pane,this.EASIROC_Pane);
		VBox vBox2= new VBox(this.Energie_Pane,this.Resolution_Pane,this.Echelle_Pane,this.MC_Pane,this.LG_HG_Pane,this.HV_Pane);
		
		SplitPane splitPane=new SplitPane(vBox1,vBox2);
		this.setContent(splitPane);
	}

}
