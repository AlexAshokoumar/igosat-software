package com.igosat.configurationPanel.controller;

import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.igosat.configurationPanel.dao.ConfigurationDAOFactory;
import com.igosat.configurationPanel.dao.ConfigurationPanelDAO;
import com.igosat.configurationPanel.gui.CPTab;
import com.igosat.configurationPanel.gui.ConfigurationPanelGUI;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * @author Alex Ashokoumar
 *
 */
public class ConfigurationPanelController extends Application {
	
	private Stage stage;
	
	private ConfigurationPanelDAO model;
	private ConfigurationPanelGUI view;

	private static Logger logger = LogManager.getLogger(ConfigurationPanelController.class);
	
	public void setConfigurationPanelController(ConfigurationPanelDAO model, ConfigurationPanelGUI view){
		this.model=model;
		this.view=view;
		
		Set<CPTab> tabList=this.view.getTabList();
		
		for(CPTab tab: tabList){
			Map<Label,TextField>map=tab.getLabelTextFieldMap();
			Set<Label> set=map.keySet();
			for(Label label:set){
				String s=label.getText().replace("/", "_");
				map.get(label).setText(""+this.model.getIntByName(s));
			}
		}
		
		this.view.saveButton.setOnAction(new SaveButtonActionHandler());//.addActionListener(new SaveButtonActionListener());
		this.view.cancelButton.setOnAction(new CancelButtonActionHandler());
	}
	
	public class SaveButtonActionHandler implements EventHandler<ActionEvent>{
		
		public void handle(ActionEvent arg0) {
			Set<CPTab> tabList=ConfigurationPanelController.this.view.getTabList();
			
			for(CPTab tab: tabList){
				Map<Label,TextField>map=tab.getLabelTextFieldMap();
				Set<Label> set=map.keySet();
				for(Label label:set){
					String s=label.getText().replace("/", "_");
					ConfigurationPanelController.this.model.setIntByName(s, Integer.parseInt(map.get(label).getText()));
				}
			}
			
			ConfigurationPanelController.this.model.saveSCIConfiguration(ConfigurationDAOFactory.defaultXMLFilePath);
		}
	}
	
	public class CancelButtonActionHandler implements EventHandler<ActionEvent>{
		
		public void handle(ActionEvent arg0) {
			ConfigurationPanelController.this.stage.close();
		}
	}
	
	/*
	public class SaveButtonActionListener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			Set<CPTab> tabList=ConfigurationPanelController.this.view.getTabList();
			
			for(CPTab tab: tabList){
				Map<Label,TextField>map=tab.getLabelTextFieldMap();
				Set<Label> set=map.keySet();
				for(Label label:set){
					String s=label.getText().replace("/", "_");
					ConfigurationPanelController.this.model.setIntByName(s, Integer.parseInt(map.get(label).getText()));
				}
			}
			
			ConfigurationPanelController.this.model.saveSCIConfiguration(ConfigurationDAOFactory.defaultXMLFilePath);
		}
		
	}
	*/
	/*
	public static void main(String[]args){
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ConfigurationPanelDAO model = ConfigurationDAOFactory.createConfigurationPanelDAO();
				ConfigurationPanelGUI view = new ConfigurationPanelGUI();
				new ConfigurationPanelController(model,view);
			}
		});
		
	}
*/
	@Override
	public void start(Stage primaryStage) throws Exception {
		try{
			
			this.stage=primaryStage;
			
			ConfigurationPanelDAO model = ConfigurationDAOFactory.createConfigurationPanelDAO();
			ConfigurationPanelGUI view = new ConfigurationPanelGUI();
			setConfigurationPanelController(model,view);
			
			this.stage.setScene(view);
			this.stage.setTitle(view.getFrameTitle());
			this.stage.show();
			logger.info("Start");
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args){
		launch(args);
	}
}
