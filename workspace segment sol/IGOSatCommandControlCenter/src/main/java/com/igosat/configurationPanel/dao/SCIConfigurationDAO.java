package com.igosat.configurationPanel.dao;

/**
 * @author Alex Ashokoumar
 *
 */
public interface SCIConfigurationDAO {
	
	public int getEmin();
	public void setEmin(int emin);
	public int getEmax();
	public void setEmax(int emax);
	
	public int getRsci();
	public void setRsci(int rsci);
	
	public int getScale();
	public void setScale(int scale);
	
	public int getV_pixel();
	public void setV_pixel(int v_pixel);
	
	public int getFT_convertisseur();
	public void setFT_convertisseur(int fT_convertisseur);
	public int getTmin_convertisseur();
	public void setTmin_convertisseur(int tmin_convertisseur);
	public int getTmax_convertisseur();
	public void setTmax_convertisseur(int tmax_convertisseur);
	
	public int getFT_SiPM();
	public void setFT_SiPM(int fT_SiPM);
	public int getTmin_SiPM();
	public void setTmin_SiPM(int tmin_SiPM);
	public int getTmax_SiPM();
	public void setTmax_SiPM(int tmax_SiPM);
	public int getFV_SiPM();
	public void setFV_SiPM(int fV_SiPM);
	public int getVmin_SiPM();
	public void setVmin_SiPM(int vmin_SiPM);
	public int getVmax_SiPM();
	public void setVmax_SiPM(int vmax_SiPM);
	
	public int getFT_EASIROC();
	public void setFT_EASIROC(int fT_EASIROC);
	public int getTmin_EASIROC();
	public void setTmin_EASIROC(int tmin_EASIROC);
	public int getTmax_EASIROC();
	public void setTmax_EASIROC(int tmax_EASIROC);
	
	public int getFT_MC();
	public void setFT_MC(int fT_MC);
	public int getTmin_MC();
	public void setTmin_MC(int tmin_MC);
	public int getTmax_MC();
	public void setTmax_MC(int tmax_MC);
	
	public int getFV_LG_HG();
	public void setFV_LG_HG(int fV_LG_HG);
	public int getVmin_LG_HG();
	public void setVmin_LG_HG(int vmin_LG_HG);
	public int getVmax_LG_HG();
	public void setVmax_LG_HG(int vmax_LG_HG);
	
	public int getFV_HV();
	public void setFV_HV(int fV_HV);
	public int getVmin_HV();
	public void setVmin_HV(int vmin_HV);
	public int getVmax_HV();
	public void setVmax_HV(int vmax_HV);
	
	public void saveSCIConfiguration(Object param);
}
