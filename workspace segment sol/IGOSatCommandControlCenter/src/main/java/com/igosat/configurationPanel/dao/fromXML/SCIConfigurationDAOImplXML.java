package com.igosat.configurationPanel.dao.fromXML;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.SCIConfigurationDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class SCIConfigurationDAOImplXML extends ConfigurationDAOImplXML implements SCIConfigurationDAO{
	
	private static final String elementNodeName="SCI";
	private static Logger logger = LogManager.getLogger(SCIConfigurationDAOImplXML.class);
	
	public SCIConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException {
		super(filePath);
		initElement(elementNodeName);
	}

	public SCIConfigurationDAOImplXML(Document document){
		super(document);
		initElement(elementNodeName);
	}
	
	public int getEmin() {
		return this.getIntFromElement("Emin");
	}

	public void setEmin(int emin) {
		this.setIntForElement("Emin", emin);
		return;
	}

	public int getEmax() {
		return this.getIntFromElement("Emax");
	}

	public void setEmax(int emax) {
		this.setIntForElement("Emax",emax);
		return;
	}

	public int getRsci() {
		return this.getIntFromElement("Rsci");
	}

	public void setRsci(int rsci) {
		this.setIntForElement("Rsci",rsci);
		return;
	}

	public int getScale() {
		return this.getIntFromElement("Scale");
	}

	public void setScale(int scale) {
		this.setIntForElement("Scale",scale);
		return;
	}

	public int getV_pixel() {
		return this.getIntFromElement("V_pixel");
	}

	public void setV_pixel(int v_pixel) {
		this.setIntForElement("V_pixel",v_pixel);
		return;
	}

	public int getFT_convertisseur() {
		return this.getIntFromElement("FT_convertisseur");
	}

	public void setFT_convertisseur(int fT_convertisseur) {
		this.setIntForElement("FT_convertisseur",fT_convertisseur);
		return;
	}

	public int getTmin_convertisseur() {
		return this.getIntFromElement("Tmin_convertisseur");
	}

	public void setTmin_convertisseur(int tmin_convertisseur) {
		this.setIntForElement("Tmin_convertisseur",tmin_convertisseur);
		return;
	}

	public int getTmax_convertisseur() {
		return this.getIntFromElement("Tmax_convertisseur");
	}

	public void setTmax_convertisseur(int tmax_convertisseur) {
		this.setIntForElement("Tmax_convertisseur",tmax_convertisseur);
		return;
	}

	public int getFT_SiPM() {
		return this.getIntFromElement("FT_SiPM");
	}

	public void setFT_SiPM(int fT_SiPM) {
		this.setIntForElement("FT_SiPM",fT_SiPM);
		return;
	}

	public int getTmin_SiPM() {
		return this.getIntFromElement("Tmin_SiPM");
	}

	public void setTmin_SiPM(int tmin_SiPM) {
		this.setIntForElement("Tmin_SiPM",tmin_SiPM);
		return;
	}

	public int getTmax_SiPM() {
		return this.getIntFromElement("Tmax_SiPM");
	}

	public void setTmax_SiPM(int tmax_SiPM) {
		this.setIntForElement("Tmax_SiPM",tmax_SiPM);
		return;
	}

	public int getFV_SiPM() {
		return this.getIntFromElement("FV_SiPM");
	}

	public void setFV_SiPM(int fV_SiPM) {
		this.setIntForElement("FV_SiPM",fV_SiPM);
		return;
	}

	public int getVmin_SiPM() {
		return this.getIntFromElement("Vmin_SiPM");
	}

	public void setVmin_SiPM(int vmin_SiPM) {
		this.setIntForElement("Vmin_SiPM",vmin_SiPM);
		return;
	}

	public int getVmax_SiPM() {
		return this.getIntFromElement("Vmax_SiPM");
	}

	public void setVmax_SiPM(int vmax_SiPM) {
		this.setIntForElement("Vmax_SiPM",vmax_SiPM);
		return;
	}

	public int getFT_EASIROC() {
		return this.getIntFromElement("FT_EASIROC");
	}

	public void setFT_EASIROC(int fT_EASIROC) {
		this.setIntForElement("FT_EASIROC",fT_EASIROC);
		return;
	}

	public int getTmin_EASIROC() {
		return this.getIntFromElement("Tmin_EASIROC");
	}

	public void setTmin_EASIROC(int tmin_EASIROC) {
		this.setIntForElement("Tmin_EASIROC",tmin_EASIROC);
		return;
	}

	public int getTmax_EASIROC() {
		return this.getIntFromElement("Tmax_EASIROC");
	}

	public void setTmax_EASIROC(int tmax_EASIROC) {
		this.setIntForElement("Tmax_EASIROC",tmax_EASIROC);
		return;
	}

	public int getFT_MC() {
		return this.getIntFromElement("FT_MC");
	}

	public void setFT_MC(int fT_MC) {
		this.setIntForElement("FT_MC",fT_MC);
		return;
	}

	public int getTmin_MC() {
		return this.getIntFromElement("Tmin_MC");
	}

	public void setTmin_MC(int tmin_MC) {
		this.setIntForElement("Tmin_MC",tmin_MC);
		return;
	}

	public int getTmax_MC() {
		return this.getIntFromElement("Tmax_MC");
	}

	public void setTmax_MC(int tmax_MC) {
		this.setIntForElement("Tmax_MC",tmax_MC);
		return;
	}

	public int getFV_LG_HG() {
		return this.getIntFromElement("FV_LG_HG");
	}

	public void setFV_LG_HG(int fV_LG_HG) {
		this.setIntForElement("FV_LG_HG",fV_LG_HG);
		return;
	}

	public int getVmin_LG_HG() {
		return this.getIntFromElement("Vmin_LG_HG");
	}

	public void setVmin_LG_HG(int vmin_LG_HG) {
		this.setIntForElement("Vmin_LG_HG",vmin_LG_HG);
		return;
	}

	public int getVmax_LG_HG() {
		return this.getIntFromElement("Vmax_LG_HG");
	}

	public void setVmax_LG_HG(int vmax_LG_HG) {
		this.setIntForElement("Vmax_LG_HG",vmax_LG_HG);
		return;
	}

	public int getFV_HV() {
		return this.getIntFromElement("FV_HV");
	}

	public void setFV_HV(int fV_HV) {
		this.setIntForElement("FV_HV",fV_HV);
		return;
	}

	public int getVmin_HV() {
		return this.getIntFromElement("Vmin_HV");
	}

	public void setVmin_HV(int vmin_HV) {
		this.setIntForElement("Vmin_HV",vmin_HV);
		return;
	}

	public int getVmax_HV() {
		return this.getIntFromElement("Vmax_HV");
	}

	public void setVmax_HV(int vmax_HV) {
		this.setIntForElement("Vmax_HV",vmax_HV);
		return;
	}
	
	public static void main(String[]args) throws SAXException, IOException, ParserConfigurationException{
		SCIConfigurationDAOImplXML a=new SCIConfigurationDAOImplXML("src/main/resources/configurationPanel.xml");
		a.setEmin(2);
		
		System.out.println("Test : "+a.getEmin());
	}

	public void saveSCIConfiguration(Object filePath) {
		if(filePath instanceof String){
			try {
				this.writeInFile((String)filePath);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//TODO else throw Exception
	}

}
