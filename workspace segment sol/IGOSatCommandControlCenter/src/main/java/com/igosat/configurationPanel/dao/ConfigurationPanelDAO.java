package com.igosat.configurationPanel.dao;

import com.igosat.configurationPanel.ConfigurationPanelParam;

/**
 * @author Alex Ashokoumar
 *
 */
public interface ConfigurationPanelDAO extends GPSConfigurationDAO,
											   SCIConfigurationDAO,
											   ODBConfigurationDAO,
											   SAEConfigurationDAO,
											   SCAOConfigurationDAO,
											   TELConfigurationDAO,
											   STRConfigurationDAO{

	public int getIntByConfigurationPanelParam(ConfigurationPanelParam param);
	public int getIntByName(String tagName);
	public void setIntByName(String tagName,int value);
}
