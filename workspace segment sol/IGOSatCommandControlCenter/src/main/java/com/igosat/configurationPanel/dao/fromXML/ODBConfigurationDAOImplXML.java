package com.igosat.configurationPanel.dao.fromXML;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.ODBConfigurationDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class ODBConfigurationDAOImplXML extends ConfigurationDAOImplXML implements ODBConfigurationDAO {

	private static final String elementNodeName="ODB";
	
	public ODBConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException {
		super(filePath);
		initElement(elementNodeName);
	}

	public ODBConfigurationDAOImplXML(Document document) {
		super(document);
		initElement(elementNodeName);
	}
	
}
