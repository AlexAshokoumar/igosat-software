package com.igosat.configurationPanel.gui;

import com.igosat.configurationPanel.ConfigurationPanelParam;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author Alex Ashokoumar
 *
 */
public class STRTab extends CPTab {

	public TitledPane STR_Pane=new TitledPane("STR",new GridPane());//(new GridLayout(3,2))
	
	public STRTab(String title) {
		super(title);
		
		GridPane gridPane;
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		
		int rowIndex=0;
		
		gridPane=(GridPane)this.STR_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_mat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_mat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_mat.getName()), new TextField(),rowIndex++);

		this.setContent(this.STR_Pane);
	}

}
