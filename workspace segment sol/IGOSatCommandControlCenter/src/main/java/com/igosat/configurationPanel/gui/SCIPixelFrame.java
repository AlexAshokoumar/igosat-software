package com.igosat.configurationPanel.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Alex Ashokoumar
 *
 */
public class SCIPixelFrame extends JFrame {

	private final static String frameTitle = "Pixel Configuration";
	private int width = 400;
	private int height = 400;
	
	public JPanel pixelPanel=new JPanel(new GridLayout(4,4));
	public JTextField[]pixelText=new JTextField[16];
	public JButton saveButton=new JButton("Save");

	public SCIPixelFrame() {
		super();

		this.setTitle(this.frameTitle);
		this.setSize(this.width, this.height);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setLayout(new BorderLayout());

		initPixelPanel();

		this.add(this.pixelPanel, BorderLayout.CENTER);
		this.add(this.saveButton, BorderLayout.PAGE_END);

		this.setVisible(true);
	}
	
	public void initPixelPanel(){
		for(int i=0;i<this.pixelText.length;i++){
			this.pixelText[i]=new JTextField();
			this.pixelPanel.add(this.pixelText[i]);
		}
	}
	
	public static void main(String[]args){
		new SCIPixelFrame();
	}
	
}
