package com.igosat.configurationPanel;

/**
 * @author Alex Ashokoumar
 *
 */
public enum ConfigurationPanelParam {

	//SCI
	Emin("Emin",11),
	Emax("Emax",11),
	Rsci("Rsci",7),
	Scale("Scale",1),
	//V_pixel("V_pixel",192),
	FT_convertisseur("FT_convertisseur",7),
	Tmin_convertisseur("Tmin_convertisseur",8),
	Tmax_convertisseur("Tmax_convertisseur",8),
	FT_SiPM("FT_SiPM",14),
	Tmin_SiPM("Tmin_SiPM",8),
	Tmax_SiPM("Tmax_SiPM",8),
	FT_EASIROC("FT_EASIROC",7),
	Tmin_EASIROC("Tmin_EASIROC",8),
	Tmax_EASIROC("Tmax_EASIROC",8),
	FT_MC("FT_MC",7),
	Tmin_MC("Tmin_MC",8),
	Tmax_MC("Tmax_MC",8),
	FV_LG_HG("FV_LG/HG",7),
	Vmin_LG_HG("Vmin_LG/HG",6),
	Vmax_LG_HG("Vmax_LG/HG",6),
	FV_HV("FV_HV",7),
	Vmin_HV("Vmin_HV",6),
	Vmax_HV("Vmax_HV",6),
	FV_SiPM("FV_SiPM",7),
	Vmin_SiPM("Vmin_SiPM",6),
	Vmax_SiPM("Vmax_SiPM",6),
	
	//GPS
	Ratio_scintillation("Ratio_scintillation",4),
	Ratio_biais("Ratio_biais",4),
	Acq_1("Acq_1",4),
	Freq_echantillonage_TEC("Freq_echantillonage_TEC",4),
	Freq_echantillonage_scintillation("Freq_echantillonage_scintillation",5),
	Freq_echantillonage_biais("Freq_echantillonage_biais",4),
	//SNR_trigger("SNR_trigger",),
	Freq_Tel_TEC("Freq_Tel_TEC",4),
	Freq_Tel_scintillation("Freq_Tel_scintillation",5),
	Freq_Tel_biais("Freq_Tel_biais",4),
	//Vect_ang("Vect_ang",),
	t_scintillation("t_scintillation",7),
	//Nb_SNR("Nb_SNR",),
	Freq_Tel_positionning("Freq_Tel_positionning",7),
	Acq_2("Acq_2",7),
	FV_antenne("FV_antenne",7),
	Vmin_antenne("Vmin_antenne",6),
	Vmax_antenne("Vmax_antenne",6),
	FT_GPS("FT_GPS",7),
	Tmin_GPS("Tmin_GPS",7),
	Tmax_GPS("Tmax_GPS",7),
	
	//SCAO
	Kp("Kp",4),
	Ki("Ki",4),
	Kd("Kd",4),
	Consign_SCA("Consign_SCA",6),
	FI_bobine("FI_bobine",7),
	Imin_bobine("Imin_bobine",4),
	Imax_bobine("Imax_bobine",4),
	FT_bobine("FT_bobine",7),
	Tmin_bobine("Tmin_bobine",7),
	Tmax_bobine("Tmax_bobine",7),
	FT_H("FT_H",7),
	Tmin_H("Tmin_H",7),
	Tmax_H("Tmax_H",7),
	FT_magnet("FT_magnet",14),
	Tmin_magnet("Tmin_magnet",7),
	Tmax_magnet("Tmax_magnet",7),
	Angle("Angle",7),
	FV_SCA("FV_SCA",7),
	Vmin_SCA("Vmin_SCA",6),
	Vmax_SCA("Vmax_SCA",6),
	Champ_F("Champ_F",7),
	//Champ_min("Champ_min",),
	//Champ_max("Champ_max",),
	
	//SAE
	FT_solar("FT_solar",7),
	Tmin_solar("Tmin_solar",7),
	Tmax_solar("Tmax_solar",7),
	FV_bat("FV_bat",7),
	Vmin_bat("Vmin_bat",6),
	Vmax_bat("Vmax_bat",6),
	FC_bat("FC_bat",7),
	//Cmin_bat("Cmin_bat",),
	//Cmax_bat("Cmax_bat",),
	Charge_bat("Charge_bat",7),
	FI_solar("FI_solar",7),
	Imin_solar("Imin_solar",4),
	Imax_solar("Imax_solar",4),
	FI_C_D("FI_C_D",14),
	Imin_C_D("Imin_C_D",4),
	Imax_C_D("Imax_C_D",4),
	FV_rail("FV_rail",7),
	Vmin_rail("Vmin_rail",6),
	Vmax_rail("Vmax_rail",6),
	FT_bat("FT_bat",7),
	Tmin_bat("Tmin_bat",7),
	Tmax_bat("Tmax_bat",7),
	Bat_min("Bat_min",7),
	
	//ODB
	FT_CPU("FT_CPU",7),
	Tmin_CPU("Tmin_CPU",7),
	Tmax_CPU("Tmax_CPU",7),
	
	//TEL
	FT_VHF_UHF("FT_VHF/UHF",7),
	Tmin_VHF_UHF("Tmin_VHF/UHF",7),
	Tmax_VHF_UHF("Tmax_VHF/UHF",7),
	FI_TX("FI_TX",7),
	Imin_TX("Imin_TX",4),
	Imax_TX("Imax_TX",4),
	FT_Amp("FT_Amp",7),
	Tmin_Amp("Tmin_Amp",7),
	Tmax_Amp("Tmax_Amp",7),
	FP_Amp("FP_Amp",7),
	//Pmin_Amp("Pmin_Amp",),
	//Pmax_Amp("Pmax_Amp",),
	FP_TX("FP_TX",7),
	//Pmin_TX("Pmin_TX",),
	//Pmax_TX("Pmax_TX",),
	NR("NR",7),
	
	//STR
	FT_mat("FT_mat",7),
	Tmin_mat("Tmin_mat",7),
	Tmax_mat("Tmax_mat",7);
	
	private String name;
	private int nbBit;

	ConfigurationPanelParam(String name, int nbBit) {
		this.name = name;
		this.nbBit = nbBit;
	}

	public String getName() {
		return this.name;
	}

	public int getNbBit() {
		return this.nbBit;
	}

	public String toString() {
		return name;
	}

}