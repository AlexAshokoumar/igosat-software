package com.igosat.configurationPanel.dao.fromXML;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.ConfigurationPanelParam;
import com.igosat.configurationPanel.dao.ConfigurationPanelDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class ConfigurationPanelDAOImplXML implements ConfigurationPanelDAO {

	private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	private DocumentBuilder builder;

	private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();
	private Transformer transformer;
	
	private Document document;

	private SCIConfigurationDAOImplXML sciConfiguration;
	private GPSConfigurationDAOImplXML gpsConfiguration;
	private ODBConfigurationDAOImplXML odbConfiguration;
	private SAEConfigurationDAOImplXML saeConfiguration;
	private SCAOConfigurationDAOImplXML scaoConfiguration;
	private STRConfigurationDAOImplXML strConfiguration;
	private TELConfigurationDAOImplXML telConfiguration;

	public ConfigurationPanelDAOImplXML(String filePath) throws ParserConfigurationException, SAXException, IOException {
		this.builder = factory.newDocumentBuilder();

		this.document = builder.parse(new File(filePath));
		this.document.getDocumentElement().normalize();
		
		this.sciConfiguration=new SCIConfigurationDAOImplXML(this.document);
		this.gpsConfiguration=new GPSConfigurationDAOImplXML(this.document);
		this.odbConfiguration=new ODBConfigurationDAOImplXML(this.document);
		this.saeConfiguration=new SAEConfigurationDAOImplXML(this.document);
		this.scaoConfiguration=new SCAOConfigurationDAOImplXML(this.document);
		this.strConfiguration=new STRConfigurationDAOImplXML(this.document);
		this.telConfiguration=new TELConfigurationDAOImplXML(this.document);
	}

	public int getEmin() {
		return this.sciConfiguration.getEmin();
	}

	public void setEmin(int emin) {
		this.sciConfiguration.setEmin(emin);
	}

	public int getEmax() {
		return this.sciConfiguration.getEmax();
	}

	public void setEmax(int emax) {
		this.sciConfiguration.setEmax(emax);
	}

	public int getRsci() {
		return this.sciConfiguration.getRsci();
	}

	public void setRsci(int rsci) {
		this.sciConfiguration.setRsci(rsci);
	}

	public int getScale() {
		return this.sciConfiguration.getScale();
	}

	public void setScale(int scale) {
		this.sciConfiguration.setScale(scale);
	}

	public int getV_pixel() {
		return this.sciConfiguration.getV_pixel();
	}

	public void setV_pixel(int v_pixel) {
		this.sciConfiguration.setV_pixel(v_pixel);
	}

	public int getFT_convertisseur() {
		return this.sciConfiguration.getFT_convertisseur();
	}

	public void setFT_convertisseur(int fT_convertisseur) {
		this.sciConfiguration.setFT_convertisseur(fT_convertisseur);
	}

	public int getTmin_convertisseur() {
		return this.sciConfiguration.getTmin_convertisseur();
	}

	public void setTmin_convertisseur(int tmin_convertisseur) {
		this.sciConfiguration.setTmin_convertisseur(tmin_convertisseur);
	}

	public int getTmax_convertisseur() {
		return this.sciConfiguration.getTmax_convertisseur();
	}

	public void setTmax_convertisseur(int tmax_convertisseur) {
		this.sciConfiguration.setTmax_convertisseur(tmax_convertisseur);
	}

	public int getFT_SiPM() {
		return this.sciConfiguration.getFT_SiPM();
	}

	public void setFT_SiPM(int fT_SiPM) {
		this.sciConfiguration.setFT_SiPM(fT_SiPM);
	}

	public int getTmin_SiPM() {
		return this.sciConfiguration.getTmin_SiPM();
	}

	public void setTmin_SiPM(int tmin_SiPM) {
		this.sciConfiguration.setTmin_SiPM(tmin_SiPM);
	}

	public int getTmax_SiPM() {
		return this.sciConfiguration.getTmax_SiPM();
	}

	public void setTmax_SiPM(int tmax_SiPM) {
		this.sciConfiguration.setTmax_SiPM(tmax_SiPM);
	}

	public int getFV_SiPM() {
		return this.sciConfiguration.getFV_SiPM();
	}

	public void setFV_SiPM(int fV_SiPM) {
		this.sciConfiguration.setFV_SiPM(fV_SiPM);
	}

	public int getVmin_SiPM() {
		return this.sciConfiguration.getVmin_SiPM();
	}

	public void setVmin_SiPM(int vmin_SiPM) {
		this.sciConfiguration.setVmin_SiPM(vmin_SiPM);
	}

	public int getVmax_SiPM() {
		return this.sciConfiguration.getVmin_SiPM();
	}

	public void setVmax_SiPM(int vmax_SiPM) {
		this.sciConfiguration.setVmax_SiPM(vmax_SiPM);
	}

	public int getFT_EASIROC() {
		return this.sciConfiguration.getFT_EASIROC();
	}

	public void setFT_EASIROC(int fT_EASIROC) {
		this.sciConfiguration.setFT_EASIROC(fT_EASIROC);
	}

	public int getTmin_EASIROC() {
		return this.sciConfiguration.getTmin_EASIROC();
	}

	public void setTmin_EASIROC(int tmin_EASIROC) {
		this.sciConfiguration.setTmin_EASIROC(tmin_EASIROC);
	}

	public int getTmax_EASIROC() {
		return this.sciConfiguration.getTmax_EASIROC();
	}

	public void setTmax_EASIROC(int tmax_EASIROC) {
		this.sciConfiguration.setTmax_EASIROC(tmax_EASIROC);
	}

	public int getFT_MC() {
		return this.sciConfiguration.getFT_MC();
	}

	public void setFT_MC(int fT_MC) {
		this.sciConfiguration.setFT_MC(fT_MC);
	}

	public int getTmin_MC() {
		return this.sciConfiguration.getTmin_MC();
	}

	public void setTmin_MC(int tmin_MC) {
		this.sciConfiguration.setTmin_MC(tmin_MC);
	}

	public int getTmax_MC() {
		return this.sciConfiguration.getTmax_MC();
	}

	public void setTmax_MC(int tmax_MC) {
		this.sciConfiguration.setTmax_MC(tmax_MC);
	}

	public int getFV_LG_HG() {
		return this.sciConfiguration.getFV_LG_HG();
	}

	public void setFV_LG_HG(int fV_LG_HG) {
		this.sciConfiguration.setFV_LG_HG(fV_LG_HG);
	}

	public int getVmin_LG_HG() {
		return this.sciConfiguration.getVmin_LG_HG();
	}

	public void setVmin_LG_HG(int vmin_LG_HG) {
		this.sciConfiguration.setVmin_LG_HG(vmin_LG_HG);
	}

	public int getVmax_LG_HG() {
		return this.sciConfiguration.getVmax_LG_HG();
	}

	public void setVmax_LG_HG(int vmax_LG_HG) {
		this.sciConfiguration.setVmax_LG_HG(vmax_LG_HG);
	}

	public int getFV_HV() {
		return this.sciConfiguration.getFV_HV();
	}

	public void setFV_HV(int fV_HV) {
		this.sciConfiguration.setFV_HV(fV_HV);
	}

	public int getVmin_HV() {
		return this.sciConfiguration.getVmin_HV();
	}

	public void setVmin_HV(int vmin_HV) {
		this.sciConfiguration.setVmin_HV(vmin_HV);
	}

	public int getVmax_HV() {
		return this.sciConfiguration.getVmax_HV();
	}

	public void setVmax_HV(int vmax_HV) {
		this.sciConfiguration.setVmax_HV(vmax_HV);
	}

	public void saveSCIConfiguration(Object filePath) {
		this.sciConfiguration.saveSCIConfiguration(filePath);
	}
	
	public int getIntByConfigurationPanelParam(ConfigurationPanelParam val) {
		return this.getIntByName(val.getName().replace('/','_'));
	}

	public int getIntByName(String tagName) {
		Node node=this.document.getElementsByTagName(tagName).item(0);//.getTextContent();
		if(node!=null){
			return Integer.parseInt(node.getTextContent());
		}
		//TODO should throw Exception
		return Integer.MIN_VALUE;
	}

	public void setIntByName(String tagName, int value) {
		Node node=this.document.getElementsByTagName(tagName).item(0);//.getTextContent();
		if(node!=null){
			node.setTextContent(""+value);
		}
		return;
	}
	
}
