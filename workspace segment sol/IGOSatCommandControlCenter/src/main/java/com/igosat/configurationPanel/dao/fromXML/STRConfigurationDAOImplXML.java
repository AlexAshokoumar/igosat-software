package com.igosat.configurationPanel.dao.fromXML;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.STRConfigurationDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class STRConfigurationDAOImplXML extends ConfigurationDAOImplXML implements STRConfigurationDAO{

	private static final String elementNodeName="STR";
	
	public STRConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException {
		super(filePath);
		initElement(elementNodeName);
	}

	public STRConfigurationDAOImplXML(Document document) {
		super(document);
		initElement(elementNodeName);
	}

}
