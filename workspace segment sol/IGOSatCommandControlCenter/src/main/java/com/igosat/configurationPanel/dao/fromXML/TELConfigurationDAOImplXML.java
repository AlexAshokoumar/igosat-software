package com.igosat.configurationPanel.dao.fromXML;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.TELConfigurationDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class TELConfigurationDAOImplXML extends ConfigurationDAOImplXML implements TELConfigurationDAO{

	private static final String elementNodeName="TEL";
	
	public TELConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException {
		super(filePath);
		// TODO Auto-generated constructor stub
	}

	public TELConfigurationDAOImplXML(Document document) {
		super(document);
		// TODO Auto-generated constructor stub
	}

}
