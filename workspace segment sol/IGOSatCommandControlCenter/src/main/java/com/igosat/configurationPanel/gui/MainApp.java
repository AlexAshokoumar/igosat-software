package com.igosat.configurationPanel.gui;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author Alex Ashokoumar
 *
 */
public class MainApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		try{
			ConfigurationPanelGUI scene=new ConfigurationPanelGUI();
			primaryStage.setScene(scene);
			primaryStage.setTitle(scene.getFrameTitle());
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args){
		launch(args);
	}
	
}
