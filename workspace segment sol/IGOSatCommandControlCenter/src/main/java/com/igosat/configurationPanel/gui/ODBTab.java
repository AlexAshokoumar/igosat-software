package com.igosat.configurationPanel.gui;

import com.igosat.configurationPanel.ConfigurationPanelParam;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author Alex Ashokoumar
 *
 */
public class ODBTab extends CPTab {

	public TitledPane ODB_Pane=new TitledPane("ODB",new GridPane());//(new GridLayout(4,2));
	
	public ODBTab(String title) {
		super(title);
		
		GridPane gridPane;
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		
		int rowIndex=0;
		
		gridPane=(GridPane)this.ODB_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_CPU.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_CPU.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_CPU.getName()), new TextField(),rowIndex++);
		
		this.setContent(this.ODB_Pane);
	}

}
