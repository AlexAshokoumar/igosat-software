package com.igosat.configurationPanel.dao.fromXML;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.SAEConfigurationDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class SAEConfigurationDAOImplXML extends ConfigurationDAOImplXML implements SAEConfigurationDAO{

	private static final String elementNodeName="SAE";
	
	public SAEConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException {
		super(filePath);
		initElement(elementNodeName);
	}

	public SAEConfigurationDAOImplXML(Document document) {
		super(document);
		initElement(elementNodeName);
	}

}
