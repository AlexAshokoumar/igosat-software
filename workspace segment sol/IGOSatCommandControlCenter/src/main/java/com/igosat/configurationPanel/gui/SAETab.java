package com.igosat.configurationPanel.gui;

import com.igosat.configurationPanel.ConfigurationPanelParam;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author Alex Ashokoumar
 *
 */
public class SAETab extends CPTab {

	public TitledPane SAE_Pane=new TitledPane("SAE",new GridPane());//(new GridLayout(23,2))
	
	public SAETab(String title) {
		super(title);
		
		GridPane gridPane;
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		
		int rowIndex=0;
		
		gridPane=(GridPane)this.SAE_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_solar.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_solar.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_solar.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FV_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmin_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmax_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FC_bat.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Cmin_bat.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Cmax_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Charge_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FI_solar.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imin_solar.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imax_solar.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FI_C_D.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imax_C_D.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imax_C_D.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FV_rail.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmin_rail.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Vmax_rail.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_bat.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Bat_min.getName()), new TextField(),rowIndex++);
		
		this.setContent(this.SAE_Pane);
	}

}
