package com.igosat.configurationPanel.dao.fromXML;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * @author Alex Ashokoumar
 *
 */
public abstract class ConfigurationDAOImplXML {

	
	private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	private DocumentBuilder builder;
	
	private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();
	private Transformer transformer;
	
	private Document document;
	protected Element element;
	
	public ConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException{
		this.builder = factory.newDocumentBuilder();
		
		this.document = builder.parse(new File(filePath));
		this.document.getDocumentElement().normalize();
	}
	
	public ConfigurationDAOImplXML(Document document){
		this.document=document;
		this.document.getDocumentElement().normalize();
	}
	
	protected void initElement(String elementName){
		Node node= this.document.getElementsByTagName(elementName).item(0);
		if (node.getNodeType() != Node.ELEMENT_NODE) {
            //TODO throw Exception
		}
		this.element= (Element) node;	
	}
	
	public String getTextFromElement(String tagName){
		return this.element.getElementsByTagName(tagName).item(0).getTextContent();
	}
	
	public void setTextForElement(String tagName, String text){
		this.element.getElementsByTagName(tagName).item(0).setTextContent(text);
		return;
	}
	
	public int getIntFromElement(String tagName){
		return Integer.parseInt(this.getTextFromElement(tagName));
	}
	
	public void setIntForElement(String tagName, int value){
		this.setTextForElement(tagName,""+value);
		return;
	}
	
	public void writeInFile(String filePath) throws TransformerException{
		if(this.transformer==null){
			this.transformer= transformerFactory.newTransformer();
		}
		DOMSource source = new DOMSource(this.document);
        StreamResult fileResult = new StreamResult(new File(filePath));
        transformer.transform(source, fileResult);
	}

}
