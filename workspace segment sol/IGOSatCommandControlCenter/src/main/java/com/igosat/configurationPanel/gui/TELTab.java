package com.igosat.configurationPanel.gui;

import com.igosat.configurationPanel.ConfigurationPanelParam;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author Alex Ashokoumar
 *
 */
public class TELTab extends CPTab {

	public TitledPane TEL_Pane=new TitledPane("TEL",new GridPane());//(new GridLayout(16,2))
	
	public TELTab(String title) {
		super(title);
		GridPane gridPane;
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		
		int rowIndex=0;
		
		gridPane=(GridPane)this.TEL_Pane.getContent();
		gridPane.getColumnConstraints().addAll(column1, column2);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_VHF_UHF.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_VHF_UHF.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_VHF_UHF.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FI_TX.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imin_TX.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Imax_TX.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FT_Amp.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmin_Amp.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Tmax_Amp.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FP_Amp.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Pmin_Amp.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Pmax_Amp.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.FP_TX.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Pmin_TX.getName()), new TextField(),rowIndex++);
		//this.addToGrid(gridPane, new Label(ConfigurationPanelParam.Pmax_TX.getName()), new TextField(),rowIndex++);
		this.addToGrid(gridPane, new Label(ConfigurationPanelParam.NR.getName()), new TextField(),rowIndex++);
		
		this.setContent(this.TEL_Pane);
	}

}
