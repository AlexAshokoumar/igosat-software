package com.igosat.configurationPanel.gui;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;


/**
 * @author Alex Ashokoumar
 *
 */
public abstract class CPTab extends Tab {
	
	protected String title;
	
	public Map<Label,TextField> labelTextFieldMap = new HashMap<Label,TextField>();
	public Map<Label,Button> labelButtonMap = new HashMap<Label,Button>();
	
	public CPTab(String title){
		super();
		this.title=title;
		this.setText(title);
		this.setClosable(false);
	}
	
	public String getTitle(){
		return this.title;
	}
	
	protected void addToGrid(GridPane pane, Label label, TextField textField, int rowIndex){
		labelTextFieldMap.put(label, textField);
		pane.addRow(rowIndex, label,textField);
	}
	
	protected void addToGrid(GridPane pane, Label label, Button button,int rowIndex){
		labelButtonMap.put(label, button);
		pane.addRow(rowIndex, label,button);
	}

	public Map<Label, TextField> getLabelTextFieldMap() {
		return labelTextFieldMap;
	}

	public Map<Label, Button> getLabelButtonMap() {
		return labelButtonMap;
	}
	
}
