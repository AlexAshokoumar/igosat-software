package com.igosat.configurationPanel.gui;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;

/**
 * @author Alex Ashokoumar
 *
 */
public class ConfigurationPanelGUI extends Scene {


	private String frameTitle = "Configuration Panel";
	private static int width = 800;
	private static int height = 650;

	private TabPane tabs = new TabPane(new SCITab("SCI"),
									   new GPSTab("GPS"),
									   new ODBTab("ODB"),
									   new SAETab("SAE"),
									   new SCAOTab("SCAO"),
									   new TELTab("TEL"),
									   new STRTab("STR")
									  );
	Set<CPTab> tabList = new LinkedHashSet<CPTab>();
	
	public static BorderPane root= new BorderPane();
	
	public ButtonBar buttonBar=new ButtonBar("key.unspecified");
	public Button saveButton=new Button("Save");
	public Button cancelButton=new Button("Cancel");
	
	private static Logger logger = LogManager.getLogger(ConfigurationPanelGUI.class);
	
	@SuppressWarnings("static-access")
	public ConfigurationPanelGUI(){
		super(root,width,height);
		this.root.setCenter(this.tabs);
		
		this.buttonBar.getButtons().add(saveButton);
		this.buttonBar.getButtons().add(cancelButton);
		root.setBottom(this.buttonBar);
		this.initTabList();
	}
	
	public Set<CPTab> getTabList() {
		return tabList;
	}
	
	public String getFrameTitle(){
		return this.frameTitle;
	}
	
	private void initTabList() {
		
		List<Tab>cpTab=this.tabs.getTabs();
		
		for (Tab tab : cpTab) {
			this.tabList.add((CPTab) tab);
		}
		return;
	}
	
	/*
	public JButton saveButton=new JButton("Save");

	public ConfigurationPanelGUI() {
		super();

		this.setTitle(this.frameTitle);
		this.setSize(this.width, this.height);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setLayout(new BorderLayout());

		initTabList();
		addTabs();

		this.add(this.saveButton, BorderLayout.PAGE_END);

		this.setVisible(true);
	}

	private void initTabList() {
		tabList.add(new SCITab("SCI"));
		tabList.add(new GPSTab("GPS"));
		tabList.add(new ODBTab("ODB"));
		tabList.add(new SAETab("SAE"));
		tabList.add(new SCAOTab("SCAO"));
		tabList.add(new TELTab("TEL"));
		tabList.add(new STRTab("STR"));
		return;
	}

	private void addTabs() {
		for (CPTab tab : tabList) {
			tabs.addTab(tab.getTitle(), tab);
		}
		this.add(tabs, BorderLayout.CENTER);
		tabs.setOpaque(true);
		return;
	}

	public static void setBestLookAndFeelAvailable() {
		String system_lf = UIManager.getSystemLookAndFeelClassName().toLowerCase();
		if (system_lf.contains("metal")) {
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			} catch (Exception e) {
			}
		} else {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception e) {
			}
		}
	}

	public Set<CPTab> getTabList() {
		return tabList;
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {

		//setBestLookAndFeelAvailable();
		/*
		LookAndFeelInfo[]tab=UIManager.getInstalledLookAndFeels();
		for(LookAndFeelInfo lf:tab){
			System.out.println(lf.getClassName());
		}
		
		//UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ConfigurationPanelGUI();
			}
		});

	}
*/
	/*
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			BorderPane root = new BorderPane(this.tabs);
			
			Scene scene = new Scene(root,this.width,this.height);
			primaryStage.setScene(scene);
			primaryStage.setTitle(this.frameTitle);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	*/
	//public static void main(String[] args){
		//Application.launch(ConfigurationPanelGUI.class, args);
		//launch(args);
	//}
	

}
