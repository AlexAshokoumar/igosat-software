package com.igosat.configurationPanel.dao.fromXML;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.GPSConfigurationDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class GPSConfigurationDAOImplXML extends ConfigurationDAOImplXML implements GPSConfigurationDAO {
	
	private static final String elementNodeName="GPS";
	
	public GPSConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException {
		super(filePath);
		initElement(elementNodeName);
	}
	
	public GPSConfigurationDAOImplXML(Document document) {
		super(document);
		initElement(elementNodeName);
	}

}
