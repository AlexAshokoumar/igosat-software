package com.igosat.configurationPanel.dao.fromXML;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.igosat.configurationPanel.dao.SCAOConfigurationDAO;

/**
 * @author Alex Ashokoumar
 *
 */
public class SCAOConfigurationDAOImplXML extends ConfigurationDAOImplXML implements SCAOConfigurationDAO{

	private static final String elementNodeName="SCAO";
	
	public SCAOConfigurationDAOImplXML(String filePath) throws SAXException, IOException, ParserConfigurationException {
		super(filePath);
		initElement(elementNodeName);
	}

	public SCAOConfigurationDAOImplXML(Document document) {
		super(document);
		initElement(elementNodeName);
	}

}
