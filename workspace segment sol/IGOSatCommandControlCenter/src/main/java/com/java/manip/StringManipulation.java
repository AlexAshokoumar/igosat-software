package com.java.manip;

/**
 * @author Alex Ashokoumar
 *
 */
public class StringManipulation {
	
	public static String reverseString(String s){
		String res="";
		for(int i=s.length()-1;i>=0;i--){
			res+=s.charAt(i);
		}
		return res;
	}
	
	private static String byteToBinaryString(byte b){
		String val =Integer.toBinaryString(b);
		int size=val.length();
		
		if(size>=8){
			return val.substring(size-8);
		}
		
		String s="";
		for(int i=0;i<(8-size);i++){
			s+="0";
		}
		return s+val;
	}
	
	public static String toBinaryString(byte[]array){
		String s="";
		for(byte b : array){
			String tmp=StringManipulation.byteToBinaryString(b);
			s+=tmp+" ";
		}
		return s;
	}
}
