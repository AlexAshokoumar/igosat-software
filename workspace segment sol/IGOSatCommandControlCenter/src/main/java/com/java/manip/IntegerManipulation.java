package com.java.manip;

/**
 * @author Alex Ashokoumar
 *
 */
public class IntegerManipulation {

	public static int getBit(int value, int pos){
	    return (value >> pos) & 1;
	}
	
	public static String intToBinaryString(int value){
		
		String val =Integer.toBinaryString(value);
		int size=val.length();
		
		String s="";
		for(int i=0;i<(Integer.SIZE-size);i++){
			s+="0";
		}
		return s+val;
	}
	
}
