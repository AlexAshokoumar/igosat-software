package test;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.IOException;

import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TestXML {

	public static void main(String[] args) throws ParserConfigurationException, TransformerException {
		
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = null;
		try {
			document = builder.parse(new File("src/main/resources/configurationPanel.xml"));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		document.getDocumentElement().normalize();

		NodeList nList = document.getElementsByTagName("SCI");
		Node nNode = nList.item(0);

		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			eElement.getElementsByTagName("Emin").item(0).setTextContent("OK");
			System.out.println("Texte : " + eElement.getElementsByTagName("Emin").item(0).getTextContent());
		}

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);
		System.out.println("-----------Modified File-----------");
		StreamResult fileResult = new StreamResult(new File("src/main/resources/configurationPanel.xml"));
		transformer.transform(source, fileResult);

		/*
		 * Code qui marche String s =
		 * document.getElementsByTagName("Mem").item(0).getTextContent();
		 * System.out.println(Float.parseFloat(s));
		 */

		/*
		 * NodeList nList = document.getElementsByTagName("ODB");
		 * 
		 * for (int temp = 0; temp < nList.getLength(); temp++) {
		 * 
		 * Node nNode = nList.item(temp);
		 * 
		 * if (nNode.getNodeType() == Node.ELEMENT_NODE) { Element eElement =
		 * (Element) nNode;
		 * 
		 * String s =
		 * eElement.getElementsByTagName("Mem").item(0).getTextContent();
		 * 
		 * System.out.println(s); } }
		 */
		/*
		 * System.out.println(racine.getNodeName());
		 * 
		 * final NodeList racineNoeuds = racine.getChildNodes();
		 * 
		 * 
		 * final int nbRacineNoeuds = racineNoeuds.getLength();
		 * 
		 * 
		 * for (int i = 0; i<nbRacineNoeuds; i++) {
		 * System.out.println(racineNoeuds.item(i).getNodeName()); }
		 * 
		 * 
		 * System.out.println(racineNoeuds.item(3).getNodeName());
		 * 
		 * 
		 * for (int i = 0; i<nbRacineNoeuds; i++) {
		 * if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) { final
		 * Node personne = racineNoeuds.item(i);
		 * System.out.println(personne.getNodeName()); } }
		 */

	}
}
