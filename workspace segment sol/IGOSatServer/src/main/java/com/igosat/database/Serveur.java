package com.igosat.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {

	private ServerSocket socket;
	
	public Serveur(int port){
		
		try {
			socket = new ServerSocket(port);
			Thread t = new Thread(new GestionnaireDeClients(socket));
			t.start();
			System.out.println("Le serveur est lancé.");

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	public static void main(String[]args){
		new Serveur(8000);
	}
	
}

class GestionnaireDeClients implements Runnable {

	private ServerSocket socketserver;
	private Socket socket;

	public GestionnaireDeClients(ServerSocket s) {
		socketserver = s;
	}

	public void run() {

		try {
			while (true) {
				socket = socketserver.accept();
				
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				
				String message = in.readLine();
				System.out.println(message);
				
				socket.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
